<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="no"/>
    <meta name="format-detection" content="telephone=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/assets/contact.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/Montserrat font/stylesheet.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="{{ asset('/assets/navbar.js') }}"></script>
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <title>Contact</title>
</head>
<body>

    @include('website._navbar')

    <div class="portfoliocontainer">
    <h1 class="tittle titlemarg">Contact us</h1>
    </div>

    <div data-aos="newfup2" data-aos-duration="800" data-aos-easing="ease-in-out-sine" class="mycontainer gmt">
      <div class="row">
        <div class="col-md-6 mt-5">
          <h4 class="infocontact">
            {{ $config->contacto_telefono }}
            <br>
            {{ $config->contacto_telefono_2 }}
            <br>
            <span>{{ $config->contacto_email }}</span>
          </h4>

          <h6 class="d-none d-md-block gmt direc">
            Baja California,
            <br class="line line-break--md-and-up" />
            México
          </h6>

          <h6 class="d-md-none gmt direc">
            Baja California, <br>
            México
          </h6>

            <a style="font-size:20px;" href="https://www.facebook.com/DHDesign-472904403518385" target="_blank" class="foottag">Facebook</a><br>
            <a style="font-size: 20px;" href="https://www.instagram.com/dhconstruccion/" target="_blank" class="foottag">Instagram</a>
            
        </div>
        <div class="col-md-6 circletm">
          <img src="{{ $img_principal->url }}" class="img-fluid gmb" alt="Responsive image">
        </div>
      </div>

    </div>
      <div class="portfoliocontainer gmt">
        <h2 class="lightmode">Get in touch</h2>
      </div>

          <section class="mycontainer mt-5 sectmb">
            <form id=col class="gmt">
         
                <div class="form-row">
                <div class="form-group col-2 col-md-2"></div>
                  <div class="form-group col-md-8 contacto">
                      <input type="text" class="form-control" id="inputname" name=name placeholder="Name" required>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-2 col-md-2"></div>
                    <div class="form-group col-md-8 contacto">
                      <input type="email" class="form-control" id="inputemail1" name="mail" placeholder="Email Address" required>
                    </div>
                </div>
                <div class="form-row mb-3">
                <div class="form-group col-md-2"></div>
                  <div class="form-group col-md-8 contacto">
                      <textarea class="form-control" id="inputmsg" name="msj" placeholder="Message"></textarea>
                  </div>
                </div>
                 
                <div class=" d-flex justify-content-center">
                  <button id="subbut" type="submit" name=submit class="send">Send</button>
                </div>			
            </form>
          </section>
          <section id="thanks">
            <div class="row d-flex justify-content-center">
              <span class="size38 gmb">
                Your message was sent succesfully. <br>
                Thank you. We'll be in touch shortly. <br>
              </span>
              </div>
              <div class="row d-flex justify-content-center sectmb">
                <a class="acontact" href="{{ route('website.contact') }}">Go back</a>
              </div>
            </div>
          </section>
          

          @include('website._footer')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

  <script>
            $("#thanks").hide();
            $(document).ready(function(){
              // Variable to hold request
            var request;
            
            // Bind to the submit event of our form
            $("#col").submit(function(event){
            
                // Prevent default posting of form - put here to work in case of errors
                event.preventDefault();
            
                // Abort any pending request
                if (request) {
                    request.abort();
                }
                // setup some local variables
                var $form = $(this);
            
                // Let's select and cache all the fields
                var $inputs = $form.find("input, select, textarea");
            
                // Serialize the data in the form
                var serializedData = $form.serialize();

                // CSRF Field
                var token = $('meta[name="csrf-token"]').attr('content'); 
            
                // Fire off the request to /form.php
                request = $.ajax({
                    url: "/mail",
                    type: "post",
                    data: serializedData,
                    headers: {
                      'X-CSRF-Token': token 
                    }
                });
            
                // Callback handler that will be called on success
                request.done(function (response, textStatus, jqXHR){
                    // Log a message to the console
                    console.log("Hooray, it worked!");
                    $("#thanks").show();
                    $("#col").hide();
                });
            
                // Callback handler that 
                request.fail(function (jqXHR, textStatus, errorThrown){
                    // Log the error to the console
                    console.error(
                        "The following error occurred: "+
                        textStatus, errorThrown
                    );
                });
            
            });
            });
            
    </script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    
    </script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>
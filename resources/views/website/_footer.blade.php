<footer>        
  <div class="footercontain mb-5">
       <div class="row .d-inline-flex">
         <div class="col-md-4 pt-0 pt-md-3 order-2 order-md-1">
           <a href="https://www.facebook.com/DHDesign-472904403518385" target="_blank" class="foottag">Facebook</a><br class="line line-break--sm-and-below">
           <a href="https://www.instagram.com/dhconstruccion/" target="_blank" class="foottag ml-md-3 ml-lg-5">Instagram</a>

         </div>
       <div class="col-md-4 text-center order-1 order-md-2 pt-3 pt-md-2">
         <svg width="12" height="40" viewBox="0 0 12 45" class="">
           <path d="M1.77692 34.5957C-0.592253 36.9935 -0.592253 40.7463 1.7688 43.1441C2.9047 44.343 4.40572 44.9999 6.0041 44.9999C7.60247 44.9999 9.10349 44.3348 10.2313 43.1441C12.6004 40.7463 12.6004 36.9853 10.2313 34.5957C7.85399 32.1979 4.14608 32.1979 1.77692 34.5957Z" fill="#1C1C1B"/>
           <path d="M12 0L0 0L0 12.1451L12 12.1451L12 0Z" fill="#1C1C1B"/>
         </svg>
       </div>
       <div class="col-md-4 pt-0 pt-md-3 order-3">
        {{ isset($config) ? $config->contacto_telefono : '' }}
       </div>

       </div>
   </div>
  </footer>
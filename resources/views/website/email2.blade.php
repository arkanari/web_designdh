<link href='https://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
<div style='color: #1c1c1b; text-align: center; background-color: #cacab0; font-family:Open Sans'>
  <img style='margin-top: 2rem;'src='/img/empresa/logo_dh.png'>
  <p style='font-size: 28px;margin-top: 100px;'>Web Message</p>
  <p>
    {{ $msj }}
  </p>
  <p style='color: #1c1c1b;'>
    {{ $name }}
    <br>
    {{ $mail }}
  </p>
  <img style='margin-top: 4rem;' src='/img/empresa/logo_compact.png'>
  <p style='font-size: 18px;color: #1c1c1b;'>Construction and Design</p>
  <div style='height: 60px;'></div>
</div>
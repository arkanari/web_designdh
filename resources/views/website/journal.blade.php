<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/assets/journal.css') }}" />
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/Montserrat font/stylesheet.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="{{ asset('/assets/navbar.js') }}"></script>
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <title>DH</title>
</head>
<body class="animate-in">

  @include('website._navbar')

    <div class="portfoliocontainer">
      <h1 class="book tittle titlemarg">Journal</h1>
    </div>

    <ul id="mywork" class="pl-0">
      @forelse($posts as $post)
        <li>
          <a href="{{ route('website.post', $post->id) }}">
            @if(count($post->fotos))
              <img src="{{ $post->fotos[0]->url }}" class="img-fluid" alt="Responsive image">
            @endif
          </a>
          <p>{{ $post->title }}</p>
        </li>
      @empty
        <li>
          <p class="text-center">There are no posts!</p>
        </li>
      @endforelse
    </ul>

    @include('website._footer')



    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    
    </script>
    <script>
      function myFunction(x) {
        x.classList.toggle("change");
      }
      window.addEventListener("beforeunload", function () {
        document.body.classList.add("animate-out");
      });
    </script>

    <script>
      firstimg=document.querySelector("img");
      firstimg.classList.add("expand-img");
    </script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
  <!-- fixed-top -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-dh">
    <a class="navbar-brand mr-auto ml-auto pl-5 d-lg-none" href="{{ route('website.home') }}">
      <svg width="75" height="24" viewBox="0 0 78 30">
      <path d="M11.3094 0H0V27.2214H11.3094C15.3873 27.2214 18.7094 25.9674 21.186 23.4893C23.6327 21.0412 24.8748 17.7171 24.8748 13.607C24.8748 9.51551 23.6327 6.17647 21.1823 3.68342C18.6944 1.24278 15.3723 0 11.3094 0ZM3.93941 3.74706H10.9204C17.2354 3.74706 20.8568 7.34439 20.8568 13.6107C20.8568 19.877 17.2354 23.4743 10.9204 23.4743H3.93941V3.74706Z" fill="#1C1C1B"/>
      <path d="M70.4904 0V11.5631H54.2763V0H50.3369V27.2214H54.2763V15.269H70.4904V27.2214H74.4298V0H70.4904Z" fill="#1C1C1B"/>
      <path d="M34.7963 19.1246C33.7039 20.2176 33.7039 21.9283 34.7926 23.0214C35.3164 23.5679 36.0085 23.8711 36.7455 23.8711C37.4825 23.8711 38.1746 23.5679 38.6946 23.0251C39.787 21.9321 39.787 20.2176 38.6946 19.1283C37.5984 18.0315 35.8888 18.0315 34.7963 19.1246Z" fill="#1C1C1B"/>
      <path d="M39.5112 3.354H33.978V8.89037H39.5112V3.354Z" fill="#1C1C1B"/>
      <defs>
      <clipPath id="clip0">
      <rect width="74.4298" height="27.2214" fill="white"/>
      </clipPath>
      </defs>
      </svg>
    </a>

    <button  onclick="myFunction(this)" id="hamburguer"class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent,#navbarSC" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <div class="bar1"></div>
      <div class="bar3"></div>
    </button>

    {{-- <button id="hamburguer"class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent,#navbarSC" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button> --}}
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-lg-auto">
        <li class="nav-item navbarY mr-auto ml-auto ml-lg-2 ml-xl-3 {{ Route::currentRouteName() == 'website.design' ? 'active' : '' }}">
          <a class="nav-link firstnavitem" href="{{ route('website.design') }}">Design</a>
        </li>
        <li class="nav-item navbarY mr-auto ml-auto ml-lg-4 {{ Route::currentRouteName() == 'website.construction' ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('website.construction') }}">Construction</a>
        </li>
    </div>

    <a class="navbar-brand mr-auto ml-auto pl-5 d-none d-lg-block" href="{{ route('website.home') }}"><svg width="75" height="24" viewBox="0 0 78 30">
      <path d="M11.3094 0H0V27.2214H11.3094C15.3873 27.2214 18.7094 25.9674 21.186 23.4893C23.6327 21.0412 24.8748 17.7171 24.8748 13.607C24.8748 9.51551 23.6327 6.17647 21.1823 3.68342C18.6944 1.24278 15.3723 0 11.3094 0ZM3.93941 3.74706H10.9204C17.2354 3.74706 20.8568 7.34439 20.8568 13.6107C20.8568 19.877 17.2354 23.4743 10.9204 23.4743H3.93941V3.74706Z" fill="#1C1C1B"/>
      <path d="M70.4904 0V11.5631H54.2763V0H50.3369V27.2214H54.2763V15.269H70.4904V27.2214H74.4298V0H70.4904Z" fill="#1C1C1B"/>
      <path d="M34.7963 19.1246C33.7039 20.2176 33.7039 21.9283 34.7926 23.0214C35.3164 23.5679 36.0085 23.8711 36.7455 23.8711C37.4825 23.8711 38.1746 23.5679 38.6946 23.0251C39.787 21.9321 39.787 20.2176 38.6946 19.1283C37.5984 18.0315 35.8888 18.0315 34.7963 19.1246Z" fill="#1C1C1B"/>
      <path d="M39.5112 3.354H33.978V8.89037H39.5112V3.354Z" fill="#1C1C1B"/>
      <defs>
      <clipPath id="clip0">
      <rect width="74.4298" height="27.2214" fill="white"/>
      </clipPath>
      </defs>
      </svg>
    </a>

    <div class="collapse navbar-collapse" id="navbarSC">
      <ul class="navbar-nav ml-lg-auto">
        <li class="nav-item navbarY mr-auto ml-auto mr-lg-4 {{ Route::currentRouteName() == 'website.portfolio' ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('website.portfolio') }}">Work</a>
        </li>
        <li class="nav-item navbarY mr-auto ml-auto mr-lg-4 {{ Route::currentRouteName() == 'website.journal' ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('website.journal') }}">Journal</a>
        </li>
        <li class="nav-item navbarY mr-auto ml-auto mr-lg-2 mr-xl-3 {{ Route::currentRouteName() == 'website.contact' ? 'active' : '' }}">
          <a href="{{ route('website.contact') }}" class="nav-link">Contact</a>
        </li>
        <li class="nav-item navbarY mr-auto ml-auto d-lg-none">
          <a href="https://www.facebook.com/DHDesign-472904403518385" target="_blank"> 
            <svg  width="10" height="20" viewBox="0 0 10 20" fill="none" class="mgfb mt-4">
              <path d="M6.66667 4.00224C7.55108 4.00224 8.46348 4.00224 9.33109 4.00224C9.33109 3.52645 9.33109 1.52253 9.33109 0C8.16681 0 6.84579 0 6.25805 0C1.91436 0 2.02071 3.50406 2.02071 4.03023C2.02071 4.15897 2.02071 5.18892 2.02071 6.66667H0V10.6689H2.02071C2.02071 15.0574 2.02071 20 2.02071 20H6.00056C6.00056 20 6.00056 14.9678 6.00056 10.6689H8.66499L9.33109 6.66667H5.99496C5.99496 5.51357 5.99496 4.74671 5.99496 4.66835C6.00056 4.23174 6.25245 4.00224 6.66667 4.00224Z" fill="#1C1C1B"/>
            </svg>
          </a>
          <a href="https://www.instagram.com/dhconstruccion/" target="_blank">
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" class="mt-4">
              <path d="M14.7294 0H5.27059C2.37059 0 0 2.42353 0 5.38235V14.6118C0 17.5765 2.37059 20 5.27059 20H14.7294C17.6294 20 20 17.5765 20 14.6176V5.38235C20 2.42353 17.6294 0 14.7294 0ZM18.2118 14.4824C18.2118 16.5353 16.5706 18.2118 14.5647 18.2118H5.44118C3.43529 18.2118 1.79412 16.5353 1.79412 14.4824V5.51765C1.79412 3.46471 3.43529 1.78824 5.44118 1.78824H14.5588C16.5647 1.78824 18.2059 3.46471 18.2059 5.51765V14.4824H18.2118Z" fill="#1C1C1B"/>
              <path d="M9.99999 4.86469C7.22352 4.86469 4.97058 7.16469 4.97058 9.99998C4.97058 12.8353 7.22352 15.1353 9.99999 15.1353C12.7765 15.1353 15.0294 12.8353 15.0294 9.99998C15.0294 7.16469 12.7765 4.86469 9.99999 4.86469ZM9.99999 13.3117C8.20588 13.3117 6.75882 11.8294 6.75882 9.99998C6.75882 8.17057 8.21176 6.68821 10.0059 6.68821C11.8 6.68821 13.2471 8.17057 13.2471 9.99998C13.2412 11.8294 11.7882 13.3117 9.99999 13.3117Z" fill="#1C1C1B"/>
              <path d="M15.3 5.74706C15.927 5.74706 16.4353 5.22824 16.4353 4.58824C16.4353 3.94824 15.927 3.42941 15.3 3.42941C14.673 3.42941 14.1647 3.94824 14.1647 4.58824C14.1647 5.22824 14.673 5.74706 15.3 5.74706Z" fill="#1C1C1B"/>
            </svg>
          </a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="navbar-start"></div>
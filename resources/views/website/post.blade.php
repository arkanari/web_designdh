<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/assets/post.css') }}">
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/Montserrat font/stylesheet.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="{{ asset('/assets/navbar.js') }}"></script>
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <title>DH</title>
</head>
<body>
  @include('website._navbar')

      <div class="postcontainer titlemarg">
        <a href="{{ route('website.journal') }}" class="gmb acontact">Back to journal</a>
      </div>

      <section class="gmb gmt mycontainer" data-aos="newfup"data-aos-duration="1500">
        <h2 class="size38">{{ $post->title }}</h2>
        @foreach($post->contenidos as $index => $contenido)
          <div class="mt-3 gmb">
            @if($contenido->tipo == 'TEXTO')
              <div class="row d-flex justify-content-between gmt">
                <div class="col-md-6">
                  <p>
                    {{ $contenido->body_1 }}
                  </p>
                </div>
                <div class="col-md-6">
                  <p class="d-flex justify-content-end">
                    {{ $contenido->body_2 }}
                  </p>
                </div>
              </div>
            @elseif($contenido->tipo == 'FOTO')
              <img src="{{ $post->contenidos[$index]->url }}" class="img-fluid gmb" alt="Responsive image">
            @endif
            @if($index == 0)
              <p class="gmt">{{ date('F j, Y', strtotime($post->fecha)) }}</p>
            @endif
          </div>
        @endforeach
      </section>

      <section class="gmb mycontainer text-center">
        <div>
          <a href="{{ $prev }}" class="acontact mr-5">Previous</a>
          <a href="{{ $next }}" class="acontact mr-3">Next</a>
        </div>
      </section>

      @include('website._footer')
      
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
          AOS.init();
        
        </script>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
            crossorigin="anonymous"
        />
        <link rel="stylesheet" href="{{ asset('/assets/construction.css') }}" />
        <link
            rel="stylesheet"
            type="text/css"
            href="{{ asset('/assets/Montserrat font/stylesheet.css') }}"
        />
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
        <script src="{{ asset('/assets/navbar.js') }}"></script>
        <link
            rel="preload"
            as="font"
            href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}"
            type="font/woff2"
            crossorigin="anonymous"
        />
        <link
            rel="preload"
            as="font"
            href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}"
            type="font/woff2"
            crossorigin="anonymous"
        />
        <title>DH</title>
    </head>
    <body>
        @include('website._navbar')

        <section class="sectmb">
            <div
                data-aos="newfup"
                data-aos-duration="1500"
                data-aos-easing="lineal"
                class="text-center"
            >
                <h1
                    style="font-size: 40px;"
                    class="text-center titlemarg lightmode"
                >
                    Construction
                </h1>
                <div class="mycontainer">
                    <h1 class="text-center lightmode size32">
                        Dream builders. We construct <br />
                        timeless investments for residential
                        <br class="line line-break--md-and-up" />
                        and commercial projects.
                    </h1>
                    <svg
                        width="90"
                        height="45"
                        viewBox="0 0 90 55"
                        fill="none"
                        class="gmb gmt"
                    >
                        <path
                            d="M30.7719 9.82227H16.0728V45.1777H30.7719C36.0731 45.1777 40.3911 43.548 43.6112 40.3294C46.7902 37.1517 48.4044 32.8275 48.4044 27.4959C48.4044 22.1807 46.7902 17.8483 43.603 14.6051C40.3747 11.4357 36.0567 9.82227 30.7719 9.82227ZM30.2721 40.313H21.1937V14.687H30.2721C38.482 14.687 43.1933 19.3552 43.1933 27.4959C43.1933 35.6366 38.482 40.313 30.2721 40.313Z"
                            fill="#1C1C1B"
                        />
                        <path
                            d="M73.9271 45.1777V9.82227H68.8062V24.8424H44.0618V29.658H68.8062V45.1777H73.9271Z"
                            fill="#1C1C1B"
                        />
                    </svg>
                </div>
            </div>
            <div class="myimgcontainer">
                <img
                    src="{{ $img_principal->url }}"
                    class="central-img img-fluid gmb"
                    alt="Responsive image"
                />

                <div class="mt-3 gmb">
                    <div
                        data-aos="newfup"
                        data-aos-duration="1000"
                        class="row d-flex justify-content-between"
                    >
                        <div class="col-md-6 gmb mb-md-0">
                            <p class="pr-md-5">
                                DH Design is a premium construction company
                                based in Mexicali, Mexico. We specialize in
                                construction, architecture and design. Since our
                                founding in 1995, we have completed over 70
                                projects. We call ourselves "dream builders"
                                because we provide exactly that.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p id="secondp" class="d-flex justify-content-end">
                                We are local experts with global know-how, ready
                                to undertake even the most demanding custom
                                projects in a reliable and transparent way. We
                                collaborate with an extensive network of
                                speciallists and can succesfully complete
                                projects at any scale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center pt-5 pt-md-3">
                <svg width="12" height="45" viewBox="0 0 12 45" fill="none">
                    <path
                        d="M1.77692 34.5957C-0.592253 36.9935 -0.592253 40.7463 1.7688 43.1441C2.9047 44.343 4.40572 44.9999 6.0041 44.9999C7.60247 44.9999 9.10349 44.3348 10.2313 43.1441C12.6004 40.7463 12.6004 36.9853 10.2313 34.5957C7.85399 32.1979 4.14608 32.1979 1.77692 34.5957Z"
                        fill="#1C1C1B"
                    />
                    <path
                        d="M12 0L0 0L0 12.1451L12 12.1451L12 0Z"
                        fill="#1C1C1B"
                    />
                </svg>
            </div>
        </section>

        <section>
            <h2 class="ml-lg-5 pl-4 lightmode">Construction Services</h2>
            <div class="mycontainer mt-5">
                <div class="row">
                    <div class="col-md-6">
                        <div
                            class="pb-md-4"
                            data-aos="newfup"
                            data-aos-duration="1000"
                        >
                            <h5 class="mt-4 cardtitle">01.</h5>
                            <h5 class="mt-lg-5 lightmode cardtitle">
                                Construction
                            </h5>
                            <h5 class="mt-lg-5 cardfont pr-1 pr-md-5">
                                Meticulous attention to construction detail. We
                                can deliver exceptional quality throught both
                                our local and non-local network of trusted
                                subcontractors and suppliers.
                            </h5>
                        </div>
                        <div
                            data-aos="newfup"
                            data-aos-duration="1500"
                            class="imgcont"
                        >
                            <img
                                src="{{ $img_proyecto[0]->url }}"
                                class="img-fluid"
                                alt="Responsive image"
                            />
                        </div>
                    </div>

                    <div id="projectm" class="col-md-6 ocasiontopm">
                        <div
                            class="pb-md-4"
                            data-aos="newfup"
                            data-aos-duration="1000"
                        >
                            <h5 class="mt-4 cardtitle">02.</h5>
                            <h5 class="mt-lg-5 lightmode cardtitle">
                                Project Managment
                            </h5>
                            <h5 class="mt-lg-5 cardfont">
                                We will closely monitor project costs, deadlines
                                and quality of construction. You will receive a detailed
                                cost breakdown, so that you can asess project
                                costs. When the budget is approved construction
                                begins.
                            </h5>
                        </div>
                        <div
                            data-aos="newfup"
                            data-aos-duration="1500"
                            class="imgcont"
                        >
                            <img
                                src="{{ $img_proyecto[1]->url }}"
                                class="img-fluid"
                                alt="Responsive image"
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div
                data-aos="newfup"
                data-aos-duration="800"
                class="text-center sectmt sectmb"
            >
                <h2 class="book ml-4 ml-md-0 mr-4 mr-md-0">
                    We offer consulting on every step of
                    <br class="line line-break--md-and-up" />
                    the process, take care of all planning,<br
                        class="line line-break--md-and-up"
                    />
                    permissions, design and construction<br
                        class="line line-break--md-and-up"
                    />
                    of your house project.
                </h2>
            </div>
        </section>
        <ul id="mywork" class="pl-0">
            <li data-aos="newfup" data-aos-duration="1500">
                <img
                    src="{{ asset('/assets/img/def/construction/construction3.jpg') }}"
                    class="img-fluid gmb"
                    alt="Responsive image"
                />
            </li>
            <li data-aos="newfup" data-aos-duration="1000">
                <img
                    src="{{ asset('/assets/img/def/construction/construction4.jpg') }}"
                    class="img-fluid gmb"
                    alt="Responsive image"
                />
            </li>

            <li data-aos="newfup" data-aos-duration="1500">
                <img
                    src="{{ asset('/assets/img/def/construction/construction5.jpg') }}"
                    class="img-fluid gmb"
                    alt="Responsive image"
                />
            </li>
            <li data-aos="newfup" data-aos-duration="1000">
                <img
                    src="{{ asset('/assets/img/def/construction/construction6.jpg') }}"
                    class="img-fluid gmb"
                    alt="Responsive image"
                />
            </li>
            <li data-aos="newfup" data-aos-duration="1500">
                <img
                    src="{{ asset('/assets/img/def/construction/construction7.jpg') }}"
                    class="img-fluid gmb"
                    alt="Responsive image"
                />
            </li>
            <li data-aos="newfup" data-aos-duration="1000">
                <img
                    src="{{ asset('/assets/img/def/construction/construction8.jpg') }}"
                    class="img-fluid gmb"
                    alt="Responsive image"
                />
            </li>
        </ul>
        <div class="container">
            <div class="contactmt" data-aos="newfup" data-aos-duration="1000">
                <div class="row justify-content-center text-center">
                    <h2>
                        Are you ready to start <br />
                        your project?
                    </h2>
                </div>
                <div class="text-center gmt sectmb">
                    <a class="acontact" href="{{ route('website.contact') }}"
                        >Contact Us</a
                    >
                </div>
            </div>
        </div>
        @include('website._footer')

        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
            AOS.init();
				</script>
				
				<script>
						function myFunction(x) {
								x.classList.toggle("change");
						}

						window.addEventListener("beforeunload", function() {
								document.body.classList.add("animate-out");
						});
				</script>

        <script
            src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"
        ></script>
    </body>
</html>

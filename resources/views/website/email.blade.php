<link href='https://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
<div style='color: #1c1c1b; text-align: center; background-color: #cacab0; font-family: Open Sans'>
  <img src='/img/empresa/logo_dh.png'>
  <p style='font-size: 28px; line-height: 0px;margin-top: 100px;'>Thank you!</p>
  <p style='font-size: 28px;'>
    We got your message and will <br>
    contact you shortly.
  </p>
  <img style='margin-top: 4rem;' src='/img/empresa/logo_compact.png'>
  <p style='font-size: 18px;'>Construction and Design</p>
  <div style='height: 60px;'></div>
</div>
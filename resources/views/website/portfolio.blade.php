<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/assets/portfolio.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/Montserrat font/stylesheet.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="{{ asset('/assets/navbar.js') }}"></script>
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <title>DH</title>
</head>
<body>

  @include('website._navbar')

     <div class="portfoliocontainer gmb titlemarg">
        <h1>Portfolio</h1>
     </div> 

    <ul id="mywork" class="pl-0">
      @foreach($images as $index => $img)
        <li data-aos="newfup" data-aos-duration="{{ $index % 2 == 0 ? '1500' : '1000' }}">
          <a href="{{ route('website.viewer') }}">
            <img src="{{ $img->url }}" class="img-fluid gmb">
          </a>
        </li>
      @endforeach
    </ul>
    <div class="gmt">
      <div class="contactmt">
        <div class="row justify-content-center text-center">
        <h2>Contact us if you <br class="line line-break--sm-and-below"> want <br class="line line-break--md-and-up">
        to see more. 
        </h2>
        </div>
        <div class="text-center gmt sectmb">
          <a class="acontact" href="{{ route('website.contact') }}">Contact Us</a>
        </div>
      </div>
      
    </div>
    

    @include('website._footer')




<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script>
  AOS.init();

</script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
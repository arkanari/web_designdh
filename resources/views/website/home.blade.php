<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <link rel="stylesheet" href="{{ asset('/assets/home.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('/assets/Montserrat font/stylesheet.css') }}">
      <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
      <script src="{{ asset('/assets/navbar.js') }}"></script>
      <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}" type="font/woff2" crossorigin="anonymous">
      <link rel="preload" as="font" href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
      <title>DH</title>
  </head>

  <body>

    @include('website._navbar')

			{{-- <video src="https://www.dropbox.com/s/5qt6nnvorbafhrp/arquitectos_chico%20%281%29.mp4?dl=1" autoplay loop playsinline muted></video> --}}
			<video src="/assets/video/video.mp4" autoplay loop playsinline muted></video>
      <div class="herodad">
      <div class="hero color-dh">
        <span class="jumbotext text-center">
          Building <br class="line-break line-break--xs-and-below">your dreams</span> 
      </div>
      </div>
  
      <div class="container mb-3">
        <div>
          <div data-aos="newfup"data-aos-duration="1000" class="row justify-content-center text-center special">
          <h1>Thoughtful design <br>
          and meticulous<br> 
          construction. </h1>
          </div>
        </div>
      </div>
  
        <section id="dhcontainer1">
          <div class="" data-aos="newfup"data-aos-duration="1500">
            <img src="{{ $img_muestra[0]->url }}" class="img-fluid" alt="Responsive image">
          </div>
  
          <div class="" data-aos="newfup"data-aos-duration="1000">
            <img src="{{ $img_muestra[1]->url }}" class="img-fluid" alt="Responsive image">
          </div>
  
          <div class="" data-aos="newfup"data-aos-duration="1500">
            <img src="{{ $img_muestra[2]->url }}" class="img-fluid" alt="Responsive image">
          </div>
          <div class="" data-aos="newfup"data-aos-duration="1000">
            <img src="{{ $img_muestra[3]->url }}" class="img-fluid" alt="Responsive image">
          </div>
        </section>
  
        <div class="container">
          <div class="gmt">
            <div class="justify-content-center text-center">
              <h2>A home should represent the way <br class="line line-break--lg-and-up">
              you want to live, the ideas that<br class="line line-break--lg-and-up">
              define your taste, perspective, <br class="line line-break--lg-and-up">
              and connections.</h2>
              <svg width="90" height="42" viewBox="0 0 90 55" fill="none"class="gmb gmt">
                <path d="M30.7719 9.82227H16.0728V45.1777H30.7719C36.0731 45.1777 40.3911 43.548 43.6112 40.3294C46.7902 37.1517 48.4044 32.8275 48.4044 27.4959C48.4044 22.1807 46.7902 17.8483 43.603 14.6051C40.3747 11.4357 36.0567 9.82227 30.7719 9.82227ZM30.2721 40.313H21.1937V14.687H30.2721C38.482 14.687 43.1933 19.3552 43.1933 27.4959C43.1933 35.6366 38.482 40.313 30.2721 40.313Z" fill="#1C1C1B"/>
                <path d="M73.9271 45.1777V9.82227H68.8062V24.8424H44.0618V29.658H68.8062V45.1777H73.9271Z" fill="#1C1C1B"/>
            </svg>
            </div>
          </div>
        </div>
  
  
      <section id="dhcontainer2" class="gmt" data-aos="newfup2" data-aos-duration="1000" data-aos-offset="200">
        @foreach($img_galeria as $img)
          <div>
            <img src="{{ $img->url }}" class="img-fluid" alt="Responsive image">
          </div>
        @endforeach
      </section>
  
      <div class="container"> 
        <div id="gallery"class="text-center sectmb gallery holo">
          <span class>
          <a class="acontact" href="{{ route('website.portfolio') }}">View full gallery</a>
          </span>
        </div>
      </div>
        
  
      <div class="container">
        <div class="mt-5" data-aos="newfup2" data-aos-duration="1500">
          <div class="row justify-content-center text-center gmb">
          <h2>Comfort, innovation and functionality <br class="line line-break--lg-and-up">
          are the driving forces behind every<br class="line line-break--lg-and-up">
          interior design project we realize.</h2>
          </div>
          <div class="text-center sectmb">
          <a class="acontact" href="{{ route('website.contact') }}">Contact Us</a>
          </div>
        </div>
      </div>

      @include('website._footer')

      
  
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
          AOS.init();
        
        </script>
  
  
  {{-- <script src="app.js"></script> --}}
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  </body>
  </html>
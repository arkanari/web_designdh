@extends ('layouts.master')

@section ('styles')

<!-- vendor css -->
<link href="/bracketv2/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/bracketv2/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/bracketv2/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
<link href="/bracketv2/lib/highlightjs/github.css" rel="stylesheet">

<style>
button:hover {
  cursor: pointer;
}
</style>

@endsection

@section ('pageheader')

<div class="br-pageheader pd-y-15 pd-l-20">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{ route('users.index') }}">Usuarios</a>
    <span class="breadcrumb-item active">Revisar Usuario</span>
  </nav>
</div><!-- br-pageheader -->

<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
  <h4 class="tx-gray-800 mg-b-5">Usuarios</h4>
  <p class="mg-b-0">Revisar Usuario</p>
</div>

@endsection

@section ('contentpanel')

@include('layouts._errors')
@include('layouts._messages')

<div class="br-section-wrapper">
  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-24 mg-b-30 section-title">
    Usuario
  </h6>

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-5 col-sm-4">
        Nombre
      </div><!-- col-4 -->
      <div class="col-7 col-sm-8">
        {{ $user->name }}
      </div><!-- col-8 -->
    </div><!-- row -->

    <div class="row no-gutters">
      <div class="col-5 col-sm-4">
        Teléfono
      </div><!-- col-4 -->
      <div class="col-7 col-sm-8">
        {{ $user->telefono }}
      </div><!-- col-8 -->
    </div><!-- row -->

    <div class="row no-gutters">
      <div class="col-5 col-sm-4">
        Correo Electrónico
      </div><!-- col-4 -->
      <div class="col-7 col-sm-8">
        {{ $user->email }}
      </div><!-- col-8 -->
    </div><!-- row -->

    <div class="row no-gutters">
      <!-- <div class="col-sm-12"> -->
        <!-- <div class="form-layout-footer"> -->
          <div class="col-5 col-sm-4 mg-l-auto">
            <a class="btn btn-outline-secondary" href="{{ route('users.index') }}">Volver</a>
          </div>        
          <div class="col-7 col-sm-8 mg-r-auto">
            @can('update', $user)
              <a href="{{ route('users.edit', $user->id) }}" class="btn btn-outline-info">Editar</a>
            @else
              <button disabled class="btn btn-outline-info">Editar</button>
            @endcan
          </div>
        <!-- </div>form-layout-footer -->
      <!-- </div>col-8 -->
    </div><!-- row -->

  </div><!-- form-layout -->
</div><!-- br-section-wrapper -->

@endsection

@section ('scripts')

<script src="/bracketv2/lib/jquery/jquery.js"></script>
<script src="/bracketv2/lib/popper.js/popper.js"></script>
<script src="/bracketv2/lib/bootstrap/bootstrap.js"></script>
<script src="/bracketv2/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/bracketv2/lib/moment/moment.js"></script>
<script src="/bracketv2/lib/jquery-ui/jquery-ui.js"></script>
<script src="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="/bracketv2/lib/peity/jquery.peity.js"></script>
<script src="/bracketv2/lib/highlightjs/highlight.pack.js"></script>

@endsection

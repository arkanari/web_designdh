@extends ('layouts.master')

@section ('styles')

<!-- vendor css -->
<link href="/bracketv2/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/bracketv2/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/bracketv2/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
<link href="/bracketv2/lib/highlightjs/github.css" rel="stylesheet">
<link href="/bracketv2/lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="/bracketv2/lib/select2/css/select2.min.css" rel="stylesheet">

<style>
.section-title {
  display: flex; 
  justify-content: space-between; 
  align-items: center
}
.table td {
  vertical-align: middle !important;
}
button:hover {
  cursor: pointer;
}
</style>

@endsection

@section ('pageheader')

<div class="br-pageheader pd-y-15 pd-l-20">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <span class="breadcrumb-item active">Usuarios</span>
  </nav>
</div><!-- br-pageheader -->

<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
  <h4 class="tx-gray-800 mg-b-5">Usuarios</h4>
</div>

@endsection

@section ('contentpanel')

@include('layouts._errors')
@include('layouts._messages')

<div class="br-section-wrapper">
  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-24 mg-b-30 section-title">
    Usuarios
    @can('create', \App\User::class)
      <a href="{{ route('users.create') }}" class="btn btn-outline-info">Crear Usuario</a>
    @endcan
  </h6>
  <div class="table-wrapper">
    <table class="table table-striped responsive compact nowrap" id="users" style="width: 100%;">
      <thead>
        <tr>
          <th>Nombre del Usuario </th>
          <th>Correo</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @foreach ($users as $user)
        <tr>
          <td>{{ $user->name }}</td>
          <td>{{ $user->email }}</td>
          <td class="table-action">
            <div class="row flex-nowrap">
              <div class="col text-center">
                <a href="{{ route('users.show', $user->id) }}" class="btn btn-outline-info btn-sm">
                  <i class="fa fa-eye"></i>
                </a>
              </div>
              <div class="col text-center">
                @if(Auth::user()->can('update', $user))
                  <a href="{{ route('users.edit', $user->id) }}" class="btn btn-outline-warning btn-sm">
                    <i class="fa fa-pencil"></i>
                  </a>
                @else
                  <button disabled class="btn btn-outline-warning btn-sm">
                    <i class="fa fa-pencil"></i>
                  </button>
                @endif
              </div>
              <div class="col text-center">
                @if(Auth::user()->can('delete', $user))
                  <form action="{{ route('users.destroy', $user->id) }}" method="POST" class="delete" data-name="{{ $user->name }}">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-outline-dark btn-sm delete-row">
                      <i class="fa fa-trash-o"></i>
                    </button>
                  </form>
                @else
                  <button disabled class="btn btn-outline-dark btn-sm">
                    <i class="fa fa-trash-o"></i>
                  </button>
                @endif
              </div>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div><!-- table-wrapper -->
</div><!-- br-section-wrapper -->

@endsection

@section ('scripts')

<script src="/bracketv2/lib/jquery/jquery.js"></script>
<script src="/bracketv2/lib/popper.js/popper.js"></script>
<script src="/bracketv2/lib/bootstrap/bootstrap.js"></script>
<script src="/bracketv2/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/bracketv2/lib/moment/moment.js"></script>
<script src="/bracketv2/lib/jquery-ui/jquery-ui.js"></script>
<script src="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="/bracketv2/lib/peity/jquery.peity.js"></script>
<script src="/bracketv2/lib/highlightjs/highlight.pack.js"></script>
<script src="/bracketv2/lib/datatables/jquery.dataTables.js"></script>
<script src="/bracketv2/lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="/bracketv2/lib/select2/js/select2.min.js"></script>

@endsection

@section ('jquery')

<script>
jQuery(document).ready(function() {

  "use strict";

  $('#users').DataTable({
    responsive: true,
    language: {
      emptyTable: "No se encontraron registros",
      info: "Mostrando registros _START_ a _END_ de _TOTAL_",
      infoEmpty: "Mostrando registros 0 a 0 de 0",
      infoFiltered: "(Filtrado de _MAX_ registros)",
      lengthMenu: '_MENU_ elementos por página',
      loadingRecords: "Cargando...",
      processing: "Procesando...",
      search: "",
      searchPlaceholder: 'Buscar...',
      zeroRecords: "No se encontraron registros",
      paginate: {
        first: "Primera",
        last: "Última",
        next: "Siguiente",
        previous: "Anterior"
      },
      aria: {
        sortAscending: ": Orden ascendente",
        sortDescending: ": Orden descendente"
      }
    }
  });

   // Select2
  jQuery('select').select2();

  $("form.delete").submit(function(event) {
    var name = $(this).data("name");
    var sure = confirm(`¿Desea remover el usuario ${name}?`);
    if(!sure)
    {
      event.preventDefault();
    }
  });

});
</script>
@endsection

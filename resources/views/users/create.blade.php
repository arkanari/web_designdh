@extends ('layouts.master')

@section ('styles')

<!-- vendor css -->
<link href="/bracketv2/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/bracketv2/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/bracketv2/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
<link href="/bracketv2/lib/highlightjs/github.css" rel="stylesheet">

<style>
form .form-layout input.is-invalid {
  border: 1px solid #dc3545 !important;
}

button:hover {
  cursor: pointer;
}
</style>

@endsection

@section ('pageheader')

<div class="br-pageheader pd-y-15 pd-l-20">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a class="breadcrumb-item" href="{{ route('users.index') }}">Usuarios</a>
    <span class="breadcrumb-item active">Creación de Usuario</span>
  </nav>
</div><!-- br-pageheader -->

<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
  <h4 class="tx-gray-800 mg-b-5">Usuarios</h4>
  <p class="mg-b-0">Creación de Usuario</p>
</div>

@endsection

@section ('contentpanel')

@include('layouts._errors')
@include('layouts._messages')

<div class="br-section-wrapper">
  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-24 mg-b-30 section-title">
    Nuevo Usuario
  </h6>
  <form action="{{ route('users.store') }}" method="POST">
    @csrf

    <div class="form-layout form-layout-7">
      <div class="row no-gutters">
        <div class="col-5 col-sm-4">
          Nombre:
        </div><!-- col-4 -->
        <div class="col-7 col-sm-8">
          <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" 
            type="text" name="name" value="{{ old('name', '') }}" 
            placeholder="Escribe el nombre" required
          >
        </div><!-- col-8 -->
      </div><!-- row -->

      <div class="row no-gutters">
        <div class="col-5 col-sm-4">
          Teléfono:
        </div><!-- col-4 -->
        <div class="col-7 col-sm-8">
          <input class="form-control {{ $errors->has('telefono') ? 'is-invalid' : '' }}" 
            type="text" name="telefono" value="{{ old('telefono','') }}" 
            placeholder="Escribe el teléfono"
          >
        </div><!-- col-8 -->
      </div><!-- row -->

      <div class="row no-gutters">
        <div class="col-5 col-sm-4">
          Correo Electrónico:
        </div><!-- col-4 -->
        <div class="col-7 col-sm-8">
          <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" 
            type="text" name="email" value="{{ old('email', '') }}" 
            placeholder="Escribe el correo electrónico" required
          >
        </div><!-- col-8 -->
      </div><!-- row -->

      <div class="row no-gutters">
        <div class="col-5 col-sm-4">
          Contraseña:
        </div><!-- col-4 -->
        <div class="col-7 col-sm-8">
          <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" 
            type="password" name="password" placeholder="Escribe la contraseña" required
          >
        </div><!-- col-8 -->
      </div><!-- row -->

      <div class="row no-gutters">
        <div class="col-5 col-sm-4">
          Confirmar Contraseña:
        </div><!-- col-4 -->
        <div class="col-7 col-sm-8">
          <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" 
            type="password" name="password_confirmation" placeholder="Escribe la contraseña" required
          >
        </div><!-- col-8 -->
      </div><!-- row -->

      <div class="row no-gutters">
        <!-- <div class="col-sm-12"> -->
          <!-- <div class="form-layout-footer"> -->
            <div class="col-5 col-sm-4 mg-l-auto">
              <a class="btn btn-outline-secondary" href="{{ route('users.index') }}">Cancelar</a>
            </div>        
            <div class="col-7 col-sm-8 mg-r-auto">
              <button class="btn btn-outline-info">Guardar</button>
            </div>
          <!-- </div>form-layout-footer -->
        <!-- </div>col-8 -->
      </div><!-- row -->

    </div><!-- form-layout -->
  </form>
</div><!-- br-section-wrapper -->

@endsection

@section ('scripts')

<script src="/bracketv2/lib/jquery/jquery.js"></script>
<script src="/bracketv2/lib/popper.js/popper.js"></script>
<script src="/bracketv2/lib/bootstrap/bootstrap.js"></script>
<script src="/bracketv2/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/bracketv2/lib/moment/moment.js"></script>
<script src="/bracketv2/lib/jquery-ui/jquery-ui.js"></script>
<script src="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="/bracketv2/lib/peity/jquery.peity.js"></script>
<script src="/bracketv2/lib/highlightjs/highlight.pack.js"></script>

@endsection

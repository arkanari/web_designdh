@extends ('layouts.master')

@section ('styles')

<!-- vendor css -->
<link href="/bracketv2/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/bracketv2/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/bracketv2/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
<link href="/bracketv2/lib/highlightjs/github.css" rel="stylesheet">

<style>
button:hover {
  cursor: pointer;
}

.image-container {
  height: 150px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 5px;
}

.foto {
  max-width: 100%;
  max-height: 100%;
}

i.foto {
  font-size: 75px;
}

.image-container.small {
  height: 100px;
}

</style>

@endsection

@section ('pageheader')

<div class="br-pageheader pd-y-15 pd-l-20">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a href="{{ route('construction.index') }}" class="breadcrumb-item">Construction</a>
    <span class="breadcrumb-item active">Editar Construction</span>
  </nav>
</div><!-- br-pageheader -->

<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
  <h4 class="tx-gray-800 mg-b-5">Construction</h4>
  <p class="mg-b-0">Editar Construction</p>
</div>

@endsection

@section ('contentpanel')

@include('layouts._errors')
@include('layouts._messages')

<form action="{{ route('construction.update') }}" class="br-section-wrapper mg-b-40" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')

  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-24 mg-b-30 section-title">
    Editar Construction Page
  </h6>

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r-1 bd-b-0">
        Foto Principal
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-2">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_principal->foto)
              <i id="missing-principal" class="fa fa-image foto d-none"></i>
              <img id="preview-principal" class="foto" src="{{ $img_principal->url }}">
            @else
              <i id="missing-principal" class="fa fa-image foto"></i>
              <img id="preview-principal" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="principal" name="principal" class="custom-file-input foto" data-target="#preview-principal" data-missing-icon="#missing-principal">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r-1 bd-b-0">
        Imágenes de Proyectos
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-2">
    <div class="row no-gutters">

      <div class="col-6 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_proyecto[0]->foto)
              <i id="missing-proyecto-0" class="fa fa-image foto d-none"></i>
              <img id="preview-proyecto-0" class="foto" src="{{ $img_proyecto[0]->url }}">
            @else
              <i id="missing-proyecto-0" class="fa fa-image foto"></i>
              <img id="preview-proyecto-0" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="proyecto-0" name="proyecto_0" class="custom-file-input foto" data-target="#preview-proyecto-0" data-missing-icon="#missing-proyecto-0">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-6 -->

      <div class="col-6 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_proyecto[1]->foto)
              <i id="missing-proyecto-1" class="fa fa-image foto d-none"></i>
              <img id="preview-proyecto-1" class="foto" src="{{ $img_proyecto[1]->url }}">
            @else
              <i id="missing-proyecto-1" class="fa fa-image foto"></i>
              <img id="preview-proyecto-1" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="proyecto-1" name="proyecto_1" class="custom-file-input foto" data-target="#preview-proyecto-1" data-missing-icon="#missing-proyecto-1">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-6 -->

    </div><!-- row -->
  </div><!-- form-layout -->

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-6">
        <a href="{{ route('construction.index') }}" class="btn btn-secondary">Cancelar</a>
      </div><!-- col-6 -->
      <div class="col-6">
        <button class="btn btn-info">
          Guardar
        </button>
      </div><!-- col-6 -->
    </div><!-- row -->
  </div><!-- form-layout -->


</form><!-- br-section-wrapper -->

@endsection

@section ('scripts')

<script src="/bracketv2/lib/jquery/jquery.js"></script>
<script src="/bracketv2/lib/popper.js/popper.js"></script>
<script src="/bracketv2/lib/bootstrap/bootstrap.js"></script>
<script src="/bracketv2/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/bracketv2/lib/moment/moment.js"></script>
<script src="/bracketv2/lib/jquery-ui/jquery-ui.js"></script>
<script src="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="/bracketv2/lib/peity/jquery.peity.js"></script>
<script src="/bracketv2/lib/highlightjs/highlight.pack.js"></script>

@endsection

@section ('jquery')
  <script>
    $(document).ready(function() {

      "use strict";

      // Preview de foto
      function readURL(input) {
        var img = $($(input).data("target"));
        var missing_icon = $($(input)).data("missing-icon");
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $(img).attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]); // convert to base64 string
          $(missing_icon).remove();
        }
      }
      $("input.foto").change(function() {
        readURL(this);
      });

    });
  </script>
@endsection
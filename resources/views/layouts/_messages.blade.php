@if(session('success'))
  <div class="alert alert-success alert-dismissable fade show">
    <strong>¡Éxito!</strong> {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div><!-- alert -->
@endif

@if(session()->has('alert'))
  <div class="alert alert-warning alert-dismissable fade show">
    <strong>¡Alerta!</strong> {{ session()->get('alert')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
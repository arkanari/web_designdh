<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="{{ env('APP_DESCRIPTION') }}">
    <meta name="author" content="{{ env('APP_AUTHOR') }}">
    <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" type="image/png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="10000;url={{ route('logout') }}" />

    <title>DH Admin</title>

    @yield ('styles')

    <!-- CSS -->
    {{-- <link href="{{ asset('/css/app.css') }}" rel="stylesheet"> --}}
    <link href="/bracketv2/css/bracket.min.css" rel="stylesheet">

  </head>

  <body class="leftpanel-collapsed collapsed-menu" oncontextmenu="return false;">
    <div id="app">
      <!-- Preloader -->
      <div id="preloader" style="position: fixed;">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
      </div>

      <section>
        @include ('layouts.leftpanel')
        @include ('layouts.headpanel')
        @include ('layouts.mainpanel')
        @include ('layouts.rightpanel')
      </section>
    </div>

    @yield('scripts')
    <!-- JS -->
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="/bracketv2/js/bracket.js"></script>

    @yield('jquery')

    @yield('vue')

  </body>
</html>

<label class="sidebar-label pd-x-15 mg-t-20">MENÚ DE OPCIONES</label>
<div class="br-sideleft-menu">

  {{-- Configuración --}}
  <a href="#" class="br-menu-link">
    <div class="br-menu-item">
      <i class="menu-item-icon fa fa-pencil tx-20"></i>
      <span class="menu-item-label">Editar Website</span>
      <i class="menu-item-arrow fa fa-angle-down"></i>
    </div><!-- menu-item -->
  </a><!-- br-menu-link -->
  <ul class="br-menu-sub nav flex-column">
    <li class="nav-item"><a href="{{ route('home.index') }}" class="nav-link">Home</a></li>
    <li class="nav-item"><a href="{{ route('contact.index') }}" class="nav-link">Contact</a></li>
    <li class="nav-item"><a href="{{ route('construction.index') }}" class="nav-link">Construction</a></li>
    <li class="nav-item"><a href="{{ route('design.index') }}" class="nav-link">Design</a></li>
    <li class="nav-item"><a href="{{ route('portfolio.index') }}" class="nav-link">Portfolio</a></li>
    <li class="nav-item"><a href="{{ route('posts.index') }}" class="nav-link">Posts</a></li>
  </ul>

  {{-- Configuración --}}
  <a href="#" class="br-menu-link">
    <div class="br-menu-item">
      <i class="menu-item-icon ion-ios-gear tx-24"></i>
      <span class="menu-item-label">Configuración</span>
      <i class="menu-item-arrow fa fa-angle-down"></i>
    </div><!-- menu-item -->
  </a><!-- br-menu-link -->
  <ul class="br-menu-sub nav flex-column">
    <li class="nav-item"><a href="{{ route('users.index') }}" class="nav-link">Usuarios</a></li>
  </ul>

  {{-- Landing Page --}}
  <a href="{{ route('website.home') }}" class="br-menu-link" target="_blank">
    <div class="br-menu-item">
      <i class="menu-item-icon icon ion-monitor tx-24"></i>
      <span class="menu-item-label">Sitio Web</span>
    </div><!-- menu-item -->
  </a><!-- br-menu-link -->
    

</div><!-- br-sideleft-menu -->

<div class="br-mainpanel">

  @yield ('pageheader')

<div class="br-pagebody">
    <!-- content goes here... -->
    <div id="app">
        @yield ('contentpanel')
    </div>
</div>

</div><!-- br-mainpanel -->
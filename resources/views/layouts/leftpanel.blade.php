<div class="br-logo">
  <a style="width: 80%; margin: 0 auto;" href="/usuarios">
    <img src="/img/empresa/logo_left_panel.png" class="w-100">
  </a>
</div><!-- br-logo -->
<div class="br-sideleft overflow-y-auto">
  @include ('layouts.navigation')
  @if(session()->get('nivel_id') == 0 || session()->get('nivel_id') == 1)
      
      {{-- @include ('layouts.infosummary')  --}}

  @endif                      
</div><!-- br-sideleft-menu -->

<!-- ########## START: RIGHT PANEL ########## -->
<div class="br-sideright">
  <ul class="nav nav-tabs sidebar-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" role="tab" href="#ordenes-pendientes"><i class="icon ion-ios-contact-outline tx-24"></i></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" role="tab" href="#ordenes-proceso"><i class="icon ion-ios-folder-outline tx-22"></i></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" role="tab" href="#ordenes-canceladas"><i class="icon ion-ios-calendar-outline tx-24"></i></a>
    </li>
  </ul><!-- sidebar-tabs -->

  <!-- Tab panes -->
  <div class="tab-content">

    <div class="tab-pane pos-absolute a-0 mg-t-60 overflow-y-auto active" id="ordenes-pendientes" role="tabpanel">
      <label class="sidebar-label pd-x-25 mg-t-25">ÓRDENES PENDIENTES</label>
    </div><!-- #ordenes-pendientes -->

    <div class="tab-pane pos-absolute a-0 mg-t-60 overflow-y-auto" id="ordenes-proceso" role="tabpanel">
      <label class="sidebar-label pd-x-25 mg-t-25">ÓRDENES EN PROCESO</label>
    </div><!-- #ordenes-proceso -->

    <div class="tab-pane pos-absolute a-0 mg-t-60 overflow-y-auto" id="ordenes-canceladas" role="tabpanel">
      <label class="sidebar-label pd-x-25 mg-t-25">ÓRDENES CANCELADAS</label>
    </div>

  </div><!-- tab-content -->
</div><!-- br-sideright -->
<!-- ########## END: RIGHT PANEL ########## --->
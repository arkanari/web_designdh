@extends ('layouts.master')

@section ('styles')

<!-- vendor css -->
<link href="/bracketv2/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/bracketv2/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/bracketv2/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
<link href="/bracketv2/lib/highlightjs/github.css" rel="stylesheet">

<style>
button:hover {
  cursor: pointer;
}

.image-container {
  height: 150px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 5px;
}

.foto {
  max-width: 100%;
  max-height: 100%;
}

i.foto {
  font-size: 75px;
}

.image-container.small {
  height: 100px;
}

</style>

@endsection

@section ('pageheader')

<div class="br-pageheader pd-y-15 pd-l-20">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a href="{{ route('home.index') }}" class="breadcrumb-item">Home</a>
    <span class="breadcrumb-item active">Editar Home</span>
  </nav>
</div><!-- br-pageheader -->

<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
  <h4 class="tx-gray-800 mg-b-5">Home</h4>
  <p class="mg-b-0">Editar Home</p>
</div>

@endsection

@section ('contentpanel')

@include('layouts._errors')
@include('layouts._messages')

<form action="{{ route('home.update') }}" class="br-section-wrapper mg-b-40" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')

  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-24 mg-b-30 section-title">
    Editar Home Page
  </h6>

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r-1 bd-b-0">
        Foto Principal
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-2">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_principal->foto)
              <i id="missing-principal" class="fa fa-image foto d-none"></i>
              <img id="preview-principal" class="foto" src="{{ asset('/assets/img/'.$img_principal->foto) }}">
            @else
              <i id="missing-principal" class="fa fa-image foto"></i>
              <img id="preview-principal" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="principal" name="principal" class="custom-file-input foto" data-target="#preview-principal" data-missing-icon="#missing-principal">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r-1 bd-b-0">
        Imágenes de muestra
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-2">
    <div class="row no-gutters">

      <div class="col-6 col-md-3 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_muestra[0]->foto)
              <i id="missing-muestra-0" class="fa fa-image foto d-none"></i>
              <img id="preview-muestra-0" class="foto" src="{{ asset('/assets/img/'.$img_muestra[0]->foto) }}">
            @else
              <i id="missing-muestra-0" class="fa fa-image foto"></i>
              <img id="preview-muestra-0" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="muestra-0" name="muestra_0" class="custom-file-input foto" data-target="#preview-muestra-0" data-missing-icon="#missing-muestra-0">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-6 -->

      <div class="col-6 col-md-3 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_muestra[1]->foto)
              <i id="missing-muestra-1" class="fa fa-image foto d-none"></i>
              <img id="preview-muestra-1" class="foto" src="{{ asset('/assets/img/'.$img_muestra[1]->foto) }}">
            @else
              <i id="missing-muestra-1" class="fa fa-image foto"></i>
              <img id="preview-muestra-1" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="muestra-1" name="muestra_1" class="custom-file-input foto" data-target="#preview-muestra-1" data-missing-icon="#missing-muestra-1">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-6 -->

      <div class="col-6 col-md-3 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_muestra[2]->foto)
              <i id="missing-muestra-2" class="fa fa-image foto d-none"></i>
              <img id="preview-muestra-2" class="foto" src="{{ asset('/assets/img/'.$img_muestra[2]->foto) }}">
            @else
              <i id="missing-muestra-2" class="fa fa-image foto"></i>
              <img id="preview-muestra-2" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="muestra-2" name="muestra_2" class="custom-file-input foto" data-target="#preview-muestra-2" data-missing-icon="#missing-muestra-2">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-6 -->

      <div class="col-6 col-md-3 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_muestra[3]->foto)
              <i id="missing-muestra-3" class="fa fa-image foto d-none"></i>
              <img id="preview-muestra-3" class="foto" src="{{ asset('/assets/img/'.$img_muestra[3]->foto) }}">
            @else
              <i id="missing-muestra-3" class="fa fa-image foto"></i>
              <img id="preview-muestra-3" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="muestra-3" name="muestra_3" class="custom-file-input foto" data-target="#preview-muestra-3" data-missing-icon="#missing-muestra-3">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-6 -->

    </div><!-- row -->
  </div><!-- form-layout -->

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r-1 bd-b-0">
        Imágenes de Galería
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-2">
    <div class="row no-gutters">

      <div class="col-4 col-md-2 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container small">
            @if($img_galeria[0]->foto)
              <i id="missing-galeria-0" class="fa fa-image foto d-none"></i>
              <img id="preview-galeria-0" class="foto" src="{{ asset('/assets/img/'.$img_galeria[0]->foto) }}">
            @else
              <i id="missing-galeria-0" class="fa fa-image foto"></i>
              <img id="preview-galeria-0" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="galeria-0" name="galeria_0" class="custom-file-input foto" data-target="#preview-galeria-0" data-missing-icon="#missing-galeria-0">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-4 -->

      <div class="col-4 col-md-2 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container small">
            @if($img_galeria[1]->foto)
              <i id="missing-galeria-1" class="fa fa-image foto d-none"></i>
              <img id="preview-galeria-1" class="foto" src="{{ asset('/assets/img/'.$img_galeria[1]->foto) }}">
            @else
              <i id="missing-galeria-1" class="fa fa-image foto"></i>
              <img id="preview-galeria-1" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="galeria-1" name="galeria_1" class="custom-file-input foto" data-target="#preview-galeria-1" data-missing-icon="#missing-galeria-1">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-4 -->

      <div class="col-4 col-md-2 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container small">
            @if($img_galeria[2]->foto)
              <i id="missing-galeria-2" class="fa fa-image foto d-none"></i>
              <img id="preview-galeria-2" class="foto" src="{{ asset('/assets/img/'.$img_galeria[2]->foto) }}">
            @else
              <i id="missing-galeria-2" class="fa fa-image foto"></i>
              <img id="preview-galeria-2" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="galeria-2" name="galeria_2" class="custom-file-input foto" data-target="#preview-galeria-2" data-missing-icon="#missing-galeria-2">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-4 -->

      <div class="col-4 col-md-2 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container small">
            @if($img_galeria[3]->foto)
              <i id="missing-galeria-3" class="fa fa-image foto d-none"></i>
              <img id="preview-galeria-3" class="foto" src="{{ asset('/assets/img/'.$img_galeria[3]->foto) }}">
            @else
              <i id="missing-galeria-3" class="fa fa-image foto"></i>
              <img id="preview-galeria-3" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="galeria-3" name="galeria_3" class="custom-file-input foto" data-target="#preview-galeria-3" data-missing-icon="#missing-galeria-3">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-4 -->

      <div class="col-4 col-md-2 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container small">
            @if($img_galeria[4]->foto)
              <i id="missing-galeria-4" class="fa fa-image foto d-none"></i>
              <img id="preview-galeria-4" class="foto" src="{{ asset('/assets/img/'.$img_galeria[4]->foto) }}">
            @else
              <i id="missing-galeria-4" class="fa fa-image foto"></i>
              <img id="preview-galeria-4" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="galeria-4" name="galeria_4" class="custom-file-input foto" data-target="#preview-galeria-4" data-missing-icon="#missing-galeria-4">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-4 -->

      <div class="col-4 col-md-2 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container small">
            @if($img_galeria[5]->foto)
              <i id="missing-galeria-5" class="fa fa-image foto d-none"></i>
              <img id="preview-galeria-5" class="foto" src="{{ asset('/assets/img/'.$img_galeria[5]->foto) }}">
            @else
              <i id="missing-galeria-5" class="fa fa-image foto"></i>
              <img id="preview-galeria-5" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="galeria-5" name="galeria_5" class="custom-file-input foto" data-target="#preview-galeria-5" data-missing-icon="#missing-galeria-5">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-4 -->

    </div><!-- row -->
  </div><!-- form-layout -->

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-6">
        <a href="{{ route('home.index') }}" class="btn btn-secondary">Cancelar</a>
      </div><!-- col-6 -->
      <div class="col-6">
        <button class="btn btn-info">
          Guardar
        </button>
      </div><!-- col-6 -->
    </div><!-- row -->
  </div><!-- form-layout -->


</form><!-- br-section-wrapper -->

@endsection

@section ('scripts')

<script src="/bracketv2/lib/jquery/jquery.js"></script>
<script src="/bracketv2/lib/popper.js/popper.js"></script>
<script src="/bracketv2/lib/bootstrap/bootstrap.js"></script>
<script src="/bracketv2/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/bracketv2/lib/moment/moment.js"></script>
<script src="/bracketv2/lib/jquery-ui/jquery-ui.js"></script>
<script src="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="/bracketv2/lib/peity/jquery.peity.js"></script>
<script src="/bracketv2/lib/highlightjs/highlight.pack.js"></script>

@endsection

@section ('jquery')
  <script>
    $(document).ready(function() {

      "use strict";

      // Preview de foto
      function readURL(input) {
        var img = $($(input).data("target"));
        var missing_icon = $($(input)).data("missing-icon");
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $(img).attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]); // convert to base64 string
          $(missing_icon).remove();
        }
      }
      $("input.foto").change(function() {
        readURL(this);
      });

    });
  </script>
@endsection
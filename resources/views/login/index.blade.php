<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
            crossorigin="anonymous"
        />
        <link
            rel="preload"
            as="font"
            href="{{ asset('/assets/Montserrat font/Montserrat-Light.woff2') }}"
            type="font/woff2"
            crossorigin="anonymous"
        />
        <link
            rel="preload"
            as="font"
            href="{{ asset('/assets/Montserrat font/Montserrat-Regular.woff2') }}"
            type="font/woff2"
            crossorigin="anonymous"
        />
        <link
            rel="stylesheet"
            type="text/css"
            href="{{ asset('/assets/Montserrat font/stylesheet.css') }}"
        />
        <link rel="stylesheet" type="text/css" href="{{ asset('/assets/login.css') }}">
        <title>Login</title>
    </head>

    <body>
        <div class="text-center mt-5 gmb">
            <svg width="120" height="43" viewBox="0 0 78 30">
                <path
                    d="M11.3094 0H0V27.2214H11.3094C15.3873 27.2214 18.7094 25.9674 21.186 23.4893C23.6327 21.0412 24.8748 17.7171 24.8748 13.607C24.8748 9.51551 23.6327 6.17647 21.1823 3.68342C18.6944 1.24278 15.3723 0 11.3094 0ZM3.93941 3.74706H10.9204C17.2354 3.74706 20.8568 7.34439 20.8568 13.6107C20.8568 19.877 17.2354 23.4743 10.9204 23.4743H3.93941V3.74706Z"
                    fill="#1C1C1B"
                />
                <path
                    d="M70.4904 0V11.5631H54.2763V0H50.3369V27.2214H54.2763V15.269H70.4904V27.2214H74.4298V0H70.4904Z"
                    fill="#1C1C1B"
                />
                <path
                    d="M34.7963 19.1246C33.7039 20.2176 33.7039 21.9283 34.7926 23.0214C35.3164 23.5679 36.0085 23.8711 36.7455 23.8711C37.4825 23.8711 38.1746 23.5679 38.6946 23.0251C39.787 21.9321 39.787 20.2176 38.6946 19.1283C37.5984 18.0315 35.8888 18.0315 34.7963 19.1246Z"
                    fill="#1C1C1B"
                />
                <path
                    d="M39.5112 3.354H33.978V8.89037H39.5112V3.354Z"
                    fill="#1C1C1B"
                />
                <defs>
                    <clipPath id="clip0">
                        <rect width="74.4298" height="27.2214" fill="white" />
                    </clipPath>
                </defs>
            </svg>
        </div>
        <div class="text-center title">
            <p>
                Welcome to DH Design and <br />
                Construction. This site is accessible<br />
                to approved users only.
            </p>
        </div>
        <section class="mycontainer mt-5">
            <form id="col" class="gmt" action="{{ route('login.login') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form col-2 col-md-4"></div>
                    <div class="col-md-4 border">
                        <input
                            type="email"
                            class="form-control"
                            id="inputname"
                            name="email"
                            placeholder="Usuario"
                            required
                        />
                    </div>
                </div>
                <div class="form-row">
                    <div class="p col-2 col-md-4"></div>
                    <div class="form-group col-md-4 border-notop">
                        <input
                            type="password"
                            class="form-control"
                            id="inputemail1"
                            name="password"
                            placeholder="Password"
                            required
                        />
                    </div>
                </div>

                <div class=" form-row gmt">
                    <div class="form-group col-2 col-md-4"></div>
                    <div class="col-md-4 border text-center">
                        <button
                            id="subbut"
                            type="submit"
                            name="submit"
                            class="send"
                        >
                            Login
                        </button>
                    </div>
                </div>
            </form>
            <div class="text-center gmt">
                <a href="{{ route('website.home') }}" class="gmb acontact"
                    >Back to design-dh.com</a
                >
            </div>
        </section>
    </body>
    <script
        src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"
    ></script>
</html>

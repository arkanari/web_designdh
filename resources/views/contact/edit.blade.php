@extends ('layouts.master')

@section ('styles')

<!-- vendor css -->
<link href="/bracketv2/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/bracketv2/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/bracketv2/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
<link href="/bracketv2/lib/highlightjs/github.css" rel="stylesheet">

<style>
button:hover {
  cursor: pointer;
}

.image-container {
  height: 150px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 5px;
}

.foto {
  max-width: 100%;
  max-height: 100%;
}

i.foto {
  font-size: 75px;
}

.image-container.small {
  height: 100px;
}

</style>

@endsection

@section ('pageheader')

<div class="br-pageheader pd-y-15 pd-l-20">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <a href="{{ route('contact.index') }}" class="breadcrumb-item">Contact</a>
    <span class="breadcrumb-item active">Editar Contact</span>
  </nav>
</div><!-- br-pageheader -->

<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
  <h4 class="tx-gray-800 mg-b-5">Contact</h4>
  <p class="mg-b-0">Editar Contact</p>
</div>

@endsection

@section ('contentpanel')

@include('layouts._errors')
@include('layouts._messages')

<form action="{{ route('contact.update') }}" class="br-section-wrapper mg-b-40" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')

  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-24 mg-b-30 section-title">
    Editar Contact Page
  </h6>

  <div class="form-layout form-layout-7">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r-1 bd-b-0">
        Foto Principal
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-2">
    <div class="row no-gutters">
      <div class="col-12 justify-content-center bd-r">
        <div class="form-group bd-r-0">
          <div class="image-container">
            @if($img_principal->foto)
              <i id="missing-principal" class="fa fa-image foto d-none"></i>
              <img id="preview-principal" class="foto" src="{{ $img_principal->url }}">
            @else
              <i id="missing-principal" class="fa fa-image foto"></i>
              <img id="preview-principal" class="foto">
            @endif
          </div><!-- image-container -->
          <label class="custom-file">
            <input type="file" id="principal" name="principal" class="custom-file-input foto" data-target="#preview-principal" data-missing-icon="#missing-principal">
            <span class="custom-file-control"></span>
          </label>
        </div><!-- form-group -->
      </div><!-- col-12 -->
    </div><!-- row -->
  </div>

  <div class="form-layout form-layout-7">

    <div class="row no-gutters">
      <div class="col-5 col-sm-4">
        Teléfono 1
      </div><!-- col-5 -->
      <div class="col-7 col-sm-8">
        <input class="form-control {{ $errors->has('contacto_telefono') ? 'is-invalid' : '' }}" 
          type="text" name="contacto_telefono" value="{{ old('contacto_telefono', $config->contacto_telefono) }}" 
          placeholder="Escribe el teléfono"
        >
      </div>
    </div><!-- row -->

    <div class="row no-gutters">
      <div class="col-5 col-sm-4">
        Teléfono 2
      </div><!-- col-5 -->
      <div class="col-7 col-sm-8">
        <input class="form-control {{ $errors->has('contacto_telefono_2') ? 'is-invalid' : '' }}" 
          type="text" name="contacto_telefono_2" value="{{ old('contacto_telefono_2', $config->contacto_telefono_2) }}" 
          placeholder="Escribe el teléfono"
        >
      </div>
    </div><!-- row -->

    <div class="row no-gutters">
      <div class="col-5 col-sm-4">
        Correo
      </div><!-- col-5 -->
      <div class="col-7 col-sm-8">
        <input class="form-control {{ $errors->has('contacto_email') ? 'is-invalid' : '' }}" 
          type="text" name="contacto_email" value="{{ old('contacto_email', $config->contacto_email) }}" 
          placeholder="Escribe el correo electrónico"
        >
      </div>
    </div><!-- row -->

    <div class="row no-gutters">
      <div class="col-6">
        <a href="{{ route('contact.index') }}" class="btn btn-secondary">Cancelar</a>
      </div><!-- col-6 -->
      <div class="col-6">
        <button class="btn btn-info">
          Guardar
        </button>
      </div><!-- col-6 -->
    </div><!-- row -->
  </div><!-- form-layout -->


</form><!-- br-section-wrapper -->

@endsection

@section ('scripts')

<script src="/bracketv2/lib/jquery/jquery.js"></script>
<script src="/bracketv2/lib/popper.js/popper.js"></script>
<script src="/bracketv2/lib/bootstrap/bootstrap.js"></script>
<script src="/bracketv2/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/bracketv2/lib/moment/moment.js"></script>
<script src="/bracketv2/lib/jquery-ui/jquery-ui.js"></script>
<script src="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="/bracketv2/lib/peity/jquery.peity.js"></script>
<script src="/bracketv2/lib/highlightjs/highlight.pack.js"></script>

@endsection

@section ('jquery')
  <script>
    $(document).ready(function() {

      "use strict";

      // Preview de foto
      function readURL(input) {
        var img = $($(input).data("target"));
        var missing_icon = $($(input)).data("missing-icon");
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $(img).attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]); // convert to base64 string
          $(missing_icon).remove();
        }
      }
      $("input.foto").change(function() {
        readURL(this);
      });

    });
  </script>
@endsection
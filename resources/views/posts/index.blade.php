@extends ('layouts.master')

@section ('styles')

<!-- vendor css -->
<link href="/bracketv2/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/bracketv2/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="/bracketv2/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
<link href="/bracketv2/lib/highlightjs/github.css" rel="stylesheet">
<link href="/bracketv2/lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="/bracketv2/lib/select2/css/select2.min.css" rel="stylesheet">

<style>
.section-title {
  display: flex; 
  justify-content: space-between; 
  align-items: center
}
.table td {
  vertical-align: middle !important;
}
button:hover {
  cursor: pointer;
}
</style>

@endsection

@section ('pageheader')

<div class="br-pageheader pd-y-15 pd-l-20">
  <nav class="breadcrumb pd-0 mg-0 tx-12">
    <span class="breadcrumb-item active">Posts</span>
  </nav>
</div><!-- br-pageheader -->

<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
  <h4 class="tx-gray-800 mg-b-5">Posts</h4>
</div>

@endsection

@section ('contentpanel')

@include('layouts._errors')
@include('layouts._messages')

<div class="br-section-wrapper">
  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-24 mg-b-30 section-title">
    Posts
    <create-post></create-post>
  </h6>
  <div class="table-wrapper">
    <table class="table table-striped responsive compact" id="posts" style="width: 100%;">
      <thead>
        <tr>
          <th>Título</th>
          <th>Fotos</th>
          <th>Párrafos</th>
          <th>Fecha</th>
          <th>Actualizado en</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @foreach ($posts as $post)
        <tr>
          <td>{{ $post->title }}</td>
          <td>{{ count($post->fotos) }}</td>
          <td>{{ count($post->textos) }}</td>
          <td>{{ $post->fecha }}</td>
          <td>{{ date('d/M/Y h:i a', strtotime($post->updated_at)) }}</td>
          <td class="table-action">
            <div class="row flex-nowrap">
              <div class="col text-center">
                <a href="{{ route('posts.show', $post->id) }}" class="btn btn-outline-info btn-sm">
                  <i class="fa fa-eye"></i>
                </a>
              </div>
              {{-- <div class="col text-center">
                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-outline-warning btn-sm">
                  <i class="fa fa-pencil"></i>
                </a>
              </div> --}}
              <div class="col text-center">
                <form action="{{ route('posts.destroy', $post->id) }}" method="POST" class="delete" data-name="{{ $post->title }}">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-outline-dark btn-sm delete-row">
                    <i class="fa fa-trash-o"></i>
                  </button>
                </form>
              </div>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div><!-- table-wrapper -->
</div><!-- br-section-wrapper -->

@endsection

@section ('scripts')

<script src="/bracketv2/lib/jquery/jquery.js"></script>
<script src="/bracketv2/lib/popper.js/popper.js"></script>
<script src="/bracketv2/lib/bootstrap/bootstrap.js"></script>
<script src="/bracketv2/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/bracketv2/lib/moment/moment.js"></script>
<script src="/bracketv2/lib/jquery-ui/jquery-ui.js"></script>
<script src="/bracketv2/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="/bracketv2/lib/peity/jquery.peity.js"></script>
<script src="/bracketv2/lib/highlightjs/highlight.pack.js"></script>
<script src="/bracketv2/lib/datatables/jquery.dataTables.js"></script>
<script src="/bracketv2/lib/datatables-responsive/dataTables.responsive.js"></script>
<script src="/bracketv2/lib/select2/js/select2.min.js"></script>

@endsection

@section ('jquery')

<script>
jQuery(document).ready(function() {

  "use strict";

  $('#posts').DataTable({
    responsive: true,
    language: {
      emptyTable: "No se encontraron registros",
      info: "Mostrando registros _START_ a _END_ de _TOTAL_",
      infoEmpty: "Mostrando registros 0 a 0 de 0",
      infoFiltered: "(Filtrado de _MAX_ registros)",
      lengthMenu: '_MENU_ elementos por página',
      loadingRecords: "Cargando...",
      processing: "Procesando...",
      search: "",
      searchPlaceholder: 'Buscar...',
      zeroRecords: "No se encontraron registros",
      paginate: {
        first: "Primera",
        last: "Última",
        next: "Siguiente",
        previous: "Anterior"
      },
      aria: {
        sortAscending: ": Orden ascendente",
        sortDescending: ": Orden descendente"
      }
    }
  });

   // Select2
  jQuery('select').select2();

  $("form.delete").submit(function(event) {
    var name = $(this).data("name");
    var sure = confirm(`¿Desea remover el Post con título ${name}?`);
    if(!sure)
    {
      event.preventDefault();
    }
  });

});
</script>
@endsection

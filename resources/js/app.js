require("./bootstrap");

window.Vue = require("vue");
import VueIziToast from "vue-izitoast";
import "izitoast/dist/css/izitoast.min.css";

Vue.use(VueIziToast);

Vue.component(
    "portfolio-page",
    require("./vue/pages/portfolio/PortfolioPage.vue").default
);
Vue.component("datepicker", require("./vue/components/Datepicker.vue").default);
Vue.component("post-page", require("./vue/pages/posts/PostPage.vue").default);
Vue.component(
    "edit-post-page",
    require("./vue/pages/posts/EditPostPage.vue").default
);
Vue.component(
    "create-post",
    require("./vue/pages/posts/CreatePost.vue").default
);

const app = new Vue({
    el: "#app"
});

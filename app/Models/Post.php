<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  use HasFactory;

  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function contenidos()
  {
    return $this->hasMany(Post_contenido::class, 'post_id', 'id');
  }

  public function getFotosAttribute()
  {
    return $this->contenidos()->where('tipo', 'FOTO')->get();
  }

  public function getTextosAttribute()
  {
    return $this->contenidos()->where('tipo', 'TEXTO')->get();
  }
}

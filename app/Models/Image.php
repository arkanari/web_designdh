<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
  use HasFactory;

  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  protected $appends = ['url'];

  public function getUrlAttribute()
  {
    return $this->foto ? "/assets/img/$this->foto" : '#';
  }
}

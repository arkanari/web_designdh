<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post_contenido extends Model
{
  use HasFactory;

  /**
   * The attributes that are not mass assignable.
   *
   * @var array
   */
  protected $guarded = ['id'];

  public function post()
  {
    return $this->belongsTo(Post::class, 'post_id', 'id');
  }

  public function getUrlAttribute()
  {
    return $this->tipo == 'FOTO' ? '/assets/img/posts/post_'.$this->post->id.'/'.$this->foto : null;
  }
}

<?php

namespace App\Http\Controllers;

use App\Models\Post_contenido;
use App\Http\Resources\PostContenidoResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PostContenidosController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $req)
  {
    $orden = Post_contenido::where('post_id', $req->post_id)
    ->latest('orden')
    ->value('orden');

    if(!$orden)
    {
      $orden = 1;
    }
    else
    {
      $orden++;
    }

    if($req->foto)
    {
      $req->validate([
        'foto' => 'required|mimes:jpg,jpeg,png|max:12048'
      ]);

      $folder = public_path('assets/img/posts').'/post_'.$req->post_id;
      $filename = 'post_'.time().'.'.$req->foto->extension();
      $contenido = Post_contenido::create([
        'post_id' => $req->post_id,
        'tipo' => 'FOTO',
        'foto' => $filename,
        'orden' => $orden
      ]);
      $req->foto->move($folder, $filename);

      return response()->json([
        'message' => 'La foto fue registrada exitosamente.',
        'contenido' => new PostcontenidoResource($contenido)
      ]);
    }
    else 
    {
      $contenido = Post_contenido::create([
        'post_id' => $req->post_id,
        'tipo' => 'TEXTO',
        'body_1' => $req->body_1,
        'body_2' => $req->body_2,
        'orden' => $orden
      ]);

      return response()->json([
        'message' => 'El texto fue registrado exitosamente.',
        'contenido' => new PostcontenidoResource($contenido)
      ]);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Post_contenido  $post_contenido
   * @return \Illuminate\Http\Response
   */
  public function update(Request $req, Post_contenido $post_contenido)
  {
    if($post_contenido->tipo == 'FOTO')
    {
      $req->validate([
        'foto' => 'required|mimes:jpg,jpeg,png|max:12048'
      ]);

      $folder = public_path('assets/img/posts').'/post_'.$post_contenido->post_id;
      $filename = 'post_'.time().'.'.$req->foto->extension();
      $old_foto = $post_contenido->foto;
      $post_contenido->update([
        'foto' => $filename
      ]);
      $req->foto->move($folder, $filename);
      File::delete("$folder/$old_foto");

      return response()->json([
        'message' => 'La foto fue actualizada exitosamente.'
      ]);
    }
    if($post_contenido->tipo == 'TEXTO')
    {
      $post_contenido->update([
        'body_1' => $req->body_1,
        'body_2' => $req->body_2
      ]);

      return response()->json([
        'message' => 'El texto fue actualizado exitosamente.'
      ]);
    }

    return response()->json([
      'message' => 'No se efectuaron cambios.'
    ]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Post_contenido  $post_contenido
   * @return \Illuminate\Http\Response
   */
  public function destroy(Post_contenido $post_contenido)
  {
    if($post_contenido->tipo == 'FOTO')
    {
      $folder = public_path('assets/img/posts').'/post_'.$post_contenido->post_id;
      $foto = $post_contenido->foto;
      File::delete("$folder/$foto");
    }
    $post_contenido->delete();

    return response()->json([
      'message' => 'El contenido fue borrado exitosamente.'
    ]);
  }
}

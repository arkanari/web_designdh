<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use App\Models\Configuracion;
use App\Models\Post;

class WebsiteController extends Controller
{
  public function home()
  {
    $img_principal = Image::where('seccion', 'HOME')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $img_muestra = Image::where('seccion', 'HOME')
    ->where('tipo', 'MUESTRA')
    ->take(4)
    ->get();

    $img_galeria = Image::where('seccion', 'HOME')
    ->where('tipo', 'GALERIA')
    ->take(6)
    ->get();

    $config = Configuracion::first();

    return view('website.home', compact('img_principal', 'img_muestra', 'img_galeria', 'config'));
  }

  public function construction()
  {
    $img_principal = Image::where('seccion', 'CONSTRUCTION')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $img_proyecto = Image::where('seccion', 'CONSTRUCTION')
    ->where('tipo', 'PROYECTO')
    ->take(2)
    ->get();

    $config = Configuracion::first();

    return view('website.construction', compact('img_principal', 'img_proyecto', 'config'));
  }

  public function contact()
  {
    $img_principal = Image::where('seccion', 'CONTACT')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $config = Configuracion::first();
    
    return view('website.contact', compact('img_principal', 'config'));
  }

  public function design()
  {
    $img_principal = Image::where('seccion', 'DESIGN')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $img_proyecto = Image::where('seccion', 'DESIGN')
    ->where('tipo', 'PROYECTO')
    ->take(2)
    ->get();

    $config = Configuracion::first();

    return view('website.design', compact('img_principal', 'img_proyecto', 'config'));
  }

  public function journal()
  {
    $posts = Post::with('contenidos')->orderByDesc('fecha')->get();

    $config = Configuracion::first();

    return view('website.journal', compact('posts', 'config'));
  }

  public function post(Post $post)
  {
    $config = Configuracion::first();

    $next_post = Post::orderBy('fecha')->where('fecha', '>', $post->fecha)->first();
    if($next_post)
    {
      $next = route('website.post', $next_post->id);
    }
    else
    {
      $next = route('website.post', Post::orderBy('fecha')->first()->id);
    }
    $prev_post = Post::orderByDesc('fecha')->where('fecha', '<', $post->fecha)->first();
    if($prev_post)
    {
      $prev = route('website.post', $prev_post->id);
    }
    else {
      $prev = route('website.post', Post::orderByDesc('fecha')->first()->id);
    }
    
    return view('website.post', compact('post', 'config', 'prev', 'next'));
  }

  public function portfolio()
  {
    $images = Image::where('seccion', 'PORTFOLIO')->get();

    $config = Configuracion::first();

    return view('website.portfolio', compact('images', 'config'));
  }

  public function viewer()
  {
    $images = Image::where('seccion', 'PORTFOLIO')->get();

    $config = Configuracion::first();

    return view('website.viewer', compact('images', 'config'));
  }

  public function test()
  {
    return view('website.test');
  }
}

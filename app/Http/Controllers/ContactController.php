<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Configuracion;
use App\Models\Image;
use Illuminate\Support\Facades\File;

class ContactController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $img_principal = Image::where('seccion', 'CONTACT')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $config = Configuracion::first();

    return view('contact.index', compact('img_principal', 'config'));
  }

  public function edit()
  {
    $img_principal = Image::where('seccion', 'CONTACT')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $config = Configuracion::first();

    return view('contact.edit', compact('img_principal', 'config'));
  }

  public function update(Request $req)
  {
    $req->validate([
      'contacto_telefono' => 'required|string|min:6|max:255',
      'contacto_telefono_2' => 'sometimes|string|min:6|max:255',
      'contacto_email' => 'required|email|max:255',
      'principal' => 'sometimes|mimes:jpg,jpeg,png|max:12048'
    ]);

    $folder_img = public_path('assets/img');

    Configuracion::first()->update([
      'contacto_telefono' => $req->contacto_telefono,
      'contacto_telefono_2' => $req->contacto_telefono_2,
      'contacto_email' => $req->contacto_email
    ]);

    if($req->principal)
    {
      $filename = 'contact_principal_'.time().'.'.$req->principal->extension();
      $img_principal = Image::where('seccion', 'CONTACT')
      ->where('tipo', 'PRINCIPAL')
      ->first();
      $old_foto = $img_principal->foto;
      $not_default = $img_principal->default == 1;
      $img_principal->update([
        'foto' => $filename,
        'default' => 1
      ]);

      $req->principal->move($folder_img, $filename);
      if($old_foto && $not_default)
      {
        File::delete("$folder_img/$old_foto");
      }
    }

    return redirect()->route('contact.index')->with('success', 'La información de la página Contact fue actualizada exitosamente.');
  }
}

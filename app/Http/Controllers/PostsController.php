<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Post_contenido;
use App\Http\Resources\PostContenidoResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $posts = Post::get();
    return view('posts.index', compact('posts'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('posts.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $req)
  {
    $rules = [
      'title' => 'required|string|min:3|max:255|unique:posts'
    ];
    
    $messages = [
      'required' => 'El campo "Título" es requerido.',
      'min' => 'El título debe tener por lo menos 3 caracteres.',
      'unique' => 'Ya existe un Post con este título.'
    ];

    $validator = Validator::make($req->all(), $rules, $messages)->validate();

    $post = Post::create([
      'title' => $req->title
    ]);

    $folder = public_path('assets/img/posts').'/post_'.$post->id;
    File::makeDirectory($folder, 0777, true, true);

    return response()->json([
      'message' => 'El Post se registró exitosamente.',
      'post_id' => $post->id
    ]);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function show(Post $post)
  {
    $post->load('contenidos');
    $contenidos = PostContenidoResource::collection($post->contenidos);
    return view('posts.show', compact('post', 'contenidos'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function edit(Post $post)
  {
    $post->load('contenidos');
    $contenidos = PostContenidoResource::collection($post->contenidos);
    return view('posts.edit', compact('post', 'contenidos'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function update(Request $req, Post $post)
  {
    if($req->title)
    {
      $rules = [
        'title' => 'required|string|min:3|max:255|unique:posts,title,'.$post->id
      ];
      $messages = [
        'required' => 'El campo "Título" es requerido.',
        'min' => 'El título debe tener por lo menos 3 caracteres.',
        'unique' => 'Ya existe un Post con este título.'
      ];
      $validator = Validator::make($req->all(), $rules, $messages)->validate();

      $post->update([
        'title' => $req->title
      ]);

      return response()->json([
        'message' => 'El Post fue actualizado exitosamente.'
      ]);
    }
    if($req->fecha)
    {
      $rules = [
        'fecha' => 'sometimes|date'
      ];
      $messages = [
        'date' => 'El valor de la fecha no es válido.',
        'min' => 'El título debe tener por lo menos 3 caracteres.',
        'unique' => 'Ya existe un Post con este título.'
      ];
      $validator = Validator::make($req->all(), $rules, $messages)->validate();

      $post->update([
        'fecha' => $req->fecha
      ]);

      return response()->json([
        'message' => 'El Post fue actualizado exitosamente.'
      ]);
    }

    return response()->json([
      'message' => 'El Post no fue actualizado.'
    ]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function destroy(Post $post)
  {
    $folder = public_path('assets/img/posts').'/post_'.$post->id;
    $contenidos = $post->contenidos;
    foreach($contenidos as $cnt)
    {
      if($cnt->tipo == 'FOTO')
      {
        File::delete("$folder/$cnt->foto");
      }
      $cnt->delete();
    }
    $post->delete();
    
    return redirect()->route('posts.index')->with('success', 'El post fue removido exitosamente.');
  }
}

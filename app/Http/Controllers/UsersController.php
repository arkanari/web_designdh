<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->authorizeResource(User::class, 'user');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = User::where('role_id', '>', 1)->get();
    return view('users.index', compact('users'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('users.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $req)
  {
    $req->validate([
      'name' => 'required|max:255',
      'email' => 'required|email|max:255|unique:users',
      'password' => 'required|confirmed',
      'telefono' => 'nullable'
    ]);

    $user = User::create([
      'name' => strtoupper($req->name),
      'email' => $req->email,
      'telefono' => $req->telefono,
      'password' => Hash::make($req->password),
      'remember_token' => Str::random(10)
    ]);

    return redirect()->route('users.show', $user->id)->with('success', 'El usuario fue registrado exitosamente.');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\User  $user
   * @return \Illuminate\Http\Response
   */
  public function show(User $user)
  {
    return view('users.show', compact('user'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\User  $user
   * @return \Illuminate\Http\Response
   */
  public function edit(User $user)
  {
    return view('users.edit', compact('user'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\User  $user
   * @return \Illuminate\Http\Response
   */
  public function update(Request $req, User $user)
  {
    $req->validate([
      'name' => 'required|max:255',
      'email' => 'required|email|max:255|unique:users,email,'.$user->id,
      'password' => 'sometimes|confirmed',
      'telefono' => 'nullable'
    ]);

    if($req->password)
    {
      $user->update([
        'name' => strtoupper($req->name),
        'email' => $req->email,
        'telefono' => $req->telefono,
        'password' => Hash::make($req->password)
      ]);
    }
    else
    {
      $user->update([
        'name' => strtoupper($req->name),
        'email' => $req->email,
        'telefono' => $req->telefono
      ]);
    }

    return redirect()->route('users.show', $user->id)->with('success', 'El usuario fue actualizado exitosamente.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\User  $user
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    $user->delete();
    return redirect()->route('users.index')->with('success', "El usuario $user->name fue removido exitosamente.");
  }
}

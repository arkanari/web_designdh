<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $img_principal = Image::where('seccion', 'HOME')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $img_muestra = Image::where('seccion', 'HOME')
    ->where('tipo', 'MUESTRA')
    ->take(4)
    ->get();

    $img_galeria = Image::where('seccion', 'HOME')
    ->where('tipo', 'GALERIA')
    ->take(6)
    ->get();

    return view('home.index', compact('img_principal', 'img_muestra', 'img_galeria'));
  }

  public function edit()
  {
    $img_principal = Image::where('seccion', 'HOME')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $img_muestra = Image::where('seccion', 'HOME')
    ->where('tipo', 'MUESTRA')
    ->take(4)
    ->get();

    $img_galeria = Image::where('seccion', 'HOME')
    ->where('tipo', 'GALERIA')
    ->take(6)
    ->get();
    
    return view('home.edit', compact('img_principal', 'img_muestra', 'img_galeria'));
  }

  public function update(Request $req)
  {
    $req->validate([
      'principal' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'muestra_0' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'muestra_1' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'muestra_2' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'muestra_3' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'galeria_0' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'galeria_1' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'galeria_2' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'galeria_3' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'galeria_4' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'galeria_5' => 'sometimes|mimes:jpg,jpeg,png|max:12048'
    ]);

    $folder_img = public_path('assets/img');

    if($req->principal)
    {
      $filename = 'home_principal_'.time().'.'.$req->principal->extension();
      $img_principal = Image::where('seccion', 'HOME')
      ->where('tipo', 'PRINCIPAL')
      ->first();
      $old_foto = $img_principal->foto;
      $not_default = $img_principal->default == 1;
      $img_principal->update([
        'foto' => $filename,
        'default' => 1
      ]);

      $req->principal->move($folder_img, $filename);
      if($old_foto && $not_default)
      {
        File::delete("$folder_img/$old_foto");
      }
    }

    $muestras = Image::where('seccion', 'HOME')
    ->where('tipo', 'MUESTRA')
    ->take(4)
    ->get();

    foreach($muestras as $index => $muestra)
    {
      if($req['muestra_'.$index])
      {
        $filename = 'home_muestra'.$index.'_'.time().'.'.$req['muestra_'.$index]->extension();
        $old_foto = $muestra->foto;
        $not_default = $muestra->default == 1;
        $muestra->update([
          'foto' => $filename,
          'default' => 1
        ]);
  
        $req['muestra_'.$index]->move($folder_img, $filename);
        if($old_foto && $not_default)
        {
          File::delete("$folder_img/$old_foto");
        }
      }
    }

    $galerias = Image::where('seccion', 'HOME')
    ->where('tipo', 'GALERIA')
    ->take(6)
    ->get();

    foreach($galerias as $index => $galeria)
    {
      if($req['galeria_'.$index])
      {
        $filename = 'home_galeria'.$index.'_'.time().'.'.$req['galeria_'.$index]->extension();
        $old_foto = $galeria->foto;
        $not_default = $galeria->default == 1;
        $galeria->update([
          'foto' => $filename,
          'default' => 1
        ]);
  
        $req['galeria_'.$index]->move($folder_img, $filename);
        if($old_foto && $not_default)
        {
          File::delete("$folder_img/$old_foto");
        }
      }
    }

    return redirect()->route('home.index')->with('success', 'La información de la página Home fue actualizada exitosamente.');
  }
}

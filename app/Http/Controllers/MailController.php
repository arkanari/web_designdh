<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{
  public function sendMail(Request $req)
  {
    $req->validate([
      'mail' => 'required|email',
      'name' => 'required',
      'msj' => 'required'
    ]);

    $mail = $req->mail;
    $name = $req->name;
    $msj = $req->msj;

    Mail::send('website.email', [
      'name' => $name,
      'mail' => $mail,
      'msj' => $msj],
      function ($message) use($mail, $name) {
        $message->from('info@design-dh.com', 'DH');
        $message->to($mail, $name)->subject('Contacto');
      }
    );

    Mail::send('website.email2', [
      'name' => $name,
      'mail' => $mail,
      'msj' => $msj],
      function ($message) use($mail, $name) {
        $message->from('info@design-dh.com', 'DH');
        $message->to('info@design-dh.com', 'DH')->subject('Contacto');
      }
    );
    
    return response()->json([
      'message' => 'El correo fue enviado exitosamente.'
    ]);
  }
}

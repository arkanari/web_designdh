<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use App\Http\Resources\ImageResource;
use Illuminate\Support\Facades\File;

class PortfolioController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $images = Image::where('seccion', 'PORTFOLIO')->get();
    $images = ImageResource::collection($images);

    return view('portfolio.index', compact('images'));
  }

  public function store(Request $req)
  {
    $req->validate([
      'image' => 'required|mimes:jpg,jpeg,png|max:12048'
    ]);

    $folder_img = public_path('assets/img');

    $filename = 'portfolio_'.time().'.'.$req->image->extension();
    $img = Image::create([
      'foto' => $filename,
      'seccion' => 'PORTFOLIO',
      'default' => 1
    ]);

    $req->image->move($folder_img, $filename);

    return new ImageResource($img);
  }

  public function destroy(Image $image)
  {
    $folder_img = public_path('assets/img');
    $filename = $image->foto;
    $not_default = $image->default == 1;
    if($filename && $not_default)
    {
      File::delete("$folder_img/$filename");
    }
    $image->delete();

    return response()->json([
      'message' => 'La imagen fue removida exitosamente.'
    ]);
  }
}

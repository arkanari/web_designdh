<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;
use Illuminate\Support\Facades\File;

class ConstructionController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $img_principal = Image::where('seccion', 'CONSTRUCTION')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $img_proyecto = Image::where('seccion', 'CONSTRUCTION')
    ->where('tipo', 'PROYECTO')
    ->take(2)
    ->get();

    return view('construction.index', compact('img_principal', 'img_proyecto'));
  }

  public function edit()
  {
    $img_principal = Image::where('seccion', 'CONSTRUCTION')
    ->where('tipo', 'PRINCIPAL')
    ->first();

    $img_proyecto = Image::where('seccion', 'CONSTRUCTION')
    ->where('tipo', 'PROYECTO')
    ->take(2)
    ->get();

    return view('construction.edit', compact('img_principal', 'img_proyecto'));
  }

  public function update(Request $req)
  {
    $req->validate([
      'principal' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'proyecto_0' => 'sometimes|mimes:jpg,jpeg,png|max:12048',
      'proyecto_1' => 'sometimes|mimes:jpg,jpeg,png|max:12048'
    ]);

    $folder_img = public_path('assets/img');

    if($req->principal)
    {
      $filename = 'construction_principal_'.time().'.'.$req->principal->extension();
      $img_principal = Image::where('seccion', 'CONSTRUCTION')
      ->where('tipo', 'PRINCIPAL')
      ->first();
      $old_foto = $img_principal->foto;
      $not_default = $img_principal->default == 1;
      $img_principal->update([
        'foto' => $filename,
        'default' => 1
      ]);

      $req->principal->move($folder_img, $filename);
      if($old_foto && $not_default)
      {
        File::delete("$folder_img/$old_foto");
      }
    }

    $proyectos = Image::where('seccion', 'CONSTRUCTION')
    ->where('tipo', 'PROYECTO')
    ->take(2)
    ->get();

    foreach($proyectos as $index => $proyecto)
    {
      if($req['proyecto_'.$index])
      {
        $filename = 'construction_proyecto'.$index.'_'.time().'.'.$req['proyecto_'.$index]->extension();
        $old_foto = $proyecto->foto;
        $not_default = $proyecto->default == 1;
        $proyecto->update([
          'foto' => $filename,
          'default' => 1
        ]);
  
        $req['proyecto_'.$index]->move($folder_img, $filename);
        if($old_foto && $not_default)
        {
          File::delete("$folder_img/$old_foto");
        }
      }
    }

    return redirect()->route('construction.index')->with('success', 'La información de la página Construction fue actualizada exitosamente.');
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Auth;
use Session;

class LoginController extends Controller
{
  public function index()
  {
    return view('login.index');
  }

  public function login(Request $req)
  {
    $req->validate([
      'email' => 'required',
      'password' => 'required'
    ]);

    if(auth()->attempt(request(['email', 'password'])) == false)
    {
      return back()->withErrors([
        'message' => 'El usuario o contraseña no es correcto.'
      ]);
    }

    Session::put('user', Auth::user());

    return redirect()->route('users.index');
  }

  public function logout()
  {
    Auth::logout();
    Session::flush();
    return redirect()->route('login.index');
  }
}

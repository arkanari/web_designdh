<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostContenidoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      if($this->tipo == 'TEXTO')
      {
        return [
          'post_id' => $this->post_id,
          'id' => $this->id,
          'type' => 'TEXTO',
          'body_1' => $this->body_1,
          'body_2' => $this->body_2
        ];
      }
      else if($this->tipo == 'FOTO')
      {
        return [
          'post_id' => $this->post_id,
          'id' => $this->id,
          'type' => 'FOTO',
          'foto' => $this->url
        ];
      }
      return [];
    }
}

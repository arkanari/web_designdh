<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Str;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('users')->insert([
      [
        'name' => 'ADRIÁN GONZÁLEZ',
        'email' => 'adrian@arkanari.com',
        'password' => Hash::make('mushin'),
        'role_id' => 0,
        'remember_token' => Str::random(10),
        'created_at' => now(),
        'updated_at' => now()
      ],
      [
        'name' => 'DESARROLLO',
        'email' => 'desarrollo@arkanari.com',
        'password' => Hash::make('mushin'),
        'role_id' => 0,
        'remember_token' => Str::random(10),
        'created_at' => now(),
        'updated_at' => now()
      ],
      [
        'name' => 'KARLA ALCALÁ',
        'email' => 'karla@agnastudio.com',
        'password' => Hash::make('123456'),
        'role_id' => 1,
        'remember_token' => Str::random(10),
        'created_at' => now(),
        'updated_at' => now()
      ],
      [
        'name' => 'MAMFRE',
        'email' => 'mamfre@agnastudio.com',
        'password' => Hash::make('123456'),
        'role_id' => 1,
        'remember_token' => Str::random(10),
        'created_at' => now(),
        'updated_at' => now()
      ],
      [
        'name' => 'INFO DESIGN-DH',
        'email' => 'info@design-dh.com',
        'password' => Hash::make('123456'),
        'role_id' => 2,
        'remember_token' => Str::random(10),
        'created_at' => now(),
        'updated_at' => now()
      ]
    ]);
  }
}

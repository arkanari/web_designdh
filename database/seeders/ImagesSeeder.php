<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    /**
     * HOME
     */
    DB::table('images')->insert([
      'foto' => 'def/index/jumbotron.png',
      'seccion' => 'HOME',
      'tipo' => 'PRINCIPAL',
      'default' => 0,
      'created_at' => now(),
      'updated_at' => now()
    ]);
    for($i = 1; $i < 5; $i++)
    {
      DB::table('images')->insert([
        'foto' => "def/index/indexg$i.png",
        'seccion' => 'HOME',
        'tipo' => 'MUESTRA',
        'default' => 0,
        'created_at' => now(),
        'updated_at' => now()
      ]);
    }
    for($i = 1; $i < 7; $i++)
    {
      DB::table('images')->insert([
        'foto' => "def/index/indexlast$i.png",
        'seccion' => 'HOME',
        'tipo' => 'GALERIA',
        'default' => 0,
        'created_at' => now(),
        'updated_at' => now()
      ]);
    }

    /**
     * DESIGN
     */
    DB::table('images')->insert([
      'foto' => 'def/design/jumbodesign.png',
      'seccion' => 'DESIGN',
      'tipo' => 'PRINCIPAL',
      'default' => 0,
      'created_at' => now(),
      'updated_at' => now()
    ]);
    for($i = 1; $i < 3; $i++)
    {
      DB::table('images')->insert([
        'foto' => "def/design/design$i.png",
        'seccion' => 'DESIGN',
        'tipo' => 'PROYECTO',
        'default' => 0,
        'created_at' => now(),
        'updated_at' => now()
      ]);
    }

    /**
     * CONSTRUCTION
     */
    DB::table('images')->insert([
      'foto' => 'def/construction/jumboconstruction.png',
      'seccion' => 'CONSTRUCTION',
      'tipo' => 'PRINCIPAL',
      'default' => 0,
      'created_at' => now(),
      'updated_at' => now()
    ]);
    for($i = 1; $i < 3; $i++)
    {
      DB::table('images')->insert([
        'foto' => "def/construction/construction$i.png",
        'seccion' => 'CONSTRUCTION',
        'tipo' => 'PROYECTO',
        'default' => 0,
        'created_at' => now(),
        'updated_at' => now()
      ]);
    }

    /**
     * CONTACT
     */
    DB::table('images')->insert([
      'foto' => 'def/contact/contact1.png',
      'seccion' => 'CONTACT',
      'tipo' => 'PRINCIPAL',
      'default' => 0,
      'created_at' => now(),
      'updated_at' => now()
    ]);

    /**
     * PORTFOLIO
     */
    for($i = 1; $i <= 26; $i++)
    {
      $extension = $i == 10 ? 'png' : 'jpg';
      DB::table('images')->insert([
        'foto' => "def/portfolio/port$i.$extension",
        'seccion' => 'PORTFOLIO',
        'default' => 0,
        'created_at' => now(),
        'updated_at' => now()
      ]);
    }
  }
}

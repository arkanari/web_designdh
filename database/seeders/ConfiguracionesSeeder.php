<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracionesSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('configuraciones')->insert([
      'contacto_telefono' => '686 212 0462',
      'contacto_email' => 'info@dhconstruccion.com',
      'created_at' => now(),
      'updated_at' => now()
    ]);
  }
}

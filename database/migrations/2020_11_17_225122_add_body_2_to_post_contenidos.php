<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBody2ToPostContenidos extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('post_contenidos', function (Blueprint $table) {
      $table->text('body_2')->nullable()->after('body');
      $table->renameColumn('body', 'body_1');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('post_contenidos', function (Blueprint $table) {
      $table->renameColumn('body_1', 'body');
      $table->dropColumn('body_2');
    });
  }
}

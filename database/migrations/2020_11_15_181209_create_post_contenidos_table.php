<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostContenidosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('post_contenidos', function (Blueprint $table) {
      $table->id();
      $table->unsignedBigInteger('post_id');
      $table->string('tipo')->default('TEXTO');
      $table->text('body')->nullable();
      $table->string('foto')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('post_contenidos');
  }
}

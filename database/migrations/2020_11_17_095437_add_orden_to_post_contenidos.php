<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Post_contenido;

class AddOrdenToPostContenidos extends Migration
{
  /**
    * Run the migrations.
    *
    * @return void
    */
  public function up()
  {
    Schema::table('post_contenidos', function (Blueprint $table) {
      $table->unsignedSmallInteger('orden')->default(0)->after('foto');
    });
    $posts_id = Post_contenido::select('post_id')
    ->groupBy('post_id')
    ->get()
    ->pluck('post_id');

    foreach($posts_id as $post_id)
    {
      $orden = 0;
      $contenidos = Post_contenido::where('post_id', $post_id)->get();
      foreach($contenidos as $cnt)
      {
        $cnt->update([
          'orden' => $orden
        ]);
        $orden++;
      }
    }
  }

  /**
    * Reverse the migrations.
    *
    * @return void
    */
  public function down()
  {
    Schema::table('post_contenidos', function (Blueprint $table) {
      $table->dropColumn('orden');
    });
  }
}

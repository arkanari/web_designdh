<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTelefono2ToConfiguracion extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('configuraciones', function (Blueprint $table) {
      $table->string('contacto_telefono_2')->nullable()->after('contacto_telefono');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('configuraciones', function (Blueprint $table) {
      $table->dropColumn('contacto_telefono_2');
    });
  }
}

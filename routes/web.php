<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebsiteController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DesignController;
use App\Http\Controllers\ConstructionController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\PostContenidosController;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WebsiteController::class, 'home'])->name('website.home');
Route::get('/construction', [WebsiteController::class, 'construction'])->name('website.construction');
Route::get('/contact', [WebsiteController::class, 'contact'])->name('website.contact');
Route::get('/design', [WebsiteController::class, 'design'])->name('website.design');
Route::get('/journal', [WebsiteController::class, 'journal'])->name('website.journal');
Route::get('/posts/{post}', [WebsiteController::class, 'post'])->name('website.post');
Route::get('/portfolio', [WebsiteController::class, 'portfolio'])->name('website.portfolio');
Route::get('/viewer', [WebsiteController::class, 'viewer'])->name('website.viewer');
Route::post('/mail', [MailController::class, 'sendMail'])->name('website.sendMail');
Route::get('/test', [WebsiteController::class, 'test'])->name('website.test');

Route::prefix('/admin')->group(function() {
  Route::get('/', function() {
    if(Auth::check())
    {
      return redirect()->route('users.index');
    }
    return redirect()->route('login.index');
  });

  Route::get('/login', [LoginController::class, 'index'])->name('login.index');
  Route::post('/login', [LoginController::class, 'login'])->name('login.login');
  Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

  Route::resource('users', UsersController::class);

  Route::get('/home', [HomeController::class, 'index'])->name('home.index');
  Route::get('/home/edit', [HomeController::class, 'edit'])->name('home.edit');
  Route::put('/home', [HomeController::class, 'update'])->name('home.update');

  Route::get('/design', [DesignController::class, 'index'])->name('design.index');
  Route::get('/design/edit', [DesignController::class, 'edit'])->name('design.edit');
  Route::put('/design', [DesignController::class, 'update'])->name('design.update');

  Route::get('/construction', [ConstructionController::class, 'index'])->name('construction.index');
  Route::get('/construction/edit', [ConstructionController::class, 'edit'])->name('construction.edit');
  Route::put('/construction', [ConstructionController::class, 'update'])->name('construction.update');

  Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
  Route::get('/contact/edit', [ContactController::class, 'edit'])->name('contact.edit');
  Route::put('/contact', [ContactController::class, 'update'])->name('contact.update');

  Route::get('/portfolio', [PortfolioController::class, 'index'])->name('portfolio.index');
  Route::post('/portfolio', [PortfolioController::class, 'store'])->name('portfolio.store');
  Route::delete('/portfolio/{image}', [PortfolioController::class, 'destroy'])->name('portfolio.destroy');

  Route::resource('posts', PostsController::class)->except('edit');
  Route::resource('post_contenidos', PostContenidosController::class)->only('store', 'update', 'destroy');
  Route::post('/post_contenidos/{post_contenido}', [PostContenidosController::class, 'update'])->name('post_contenidos.update2');
});